

PRE-REQUISITES
-----------------
1. Java 1.7 or above installed and added to path 
2. Direct access to vCenter and HTCC environments from machine on which utility is run


RUNNING THE UTILITY
-----------------
./run-benchmark.sh <output-file-name> <with/without-pip> <write-detailed-operation-results> <files-to-write-detailed-operation-result>

OPTIONS
-----------------
output-file-name : It is the file where results of performance benchmark run will be written
with/without-pip : (true/false) Boolean value specifying whether current run of performance benchmark is through HTCC or without HTCC.
write-detailed-operation-results : (true/false) Boolean value to specify whether to write all operations detailed result to an output file.
files-to-write-detailed-operation-result : Filename to write all operations detailed results.

Note: 
1. First three parameters are mandatory. If <write-detailed-operation-results> is true, a file path has to be provided to write detailed 
operations output.
2. The script serializes the result in a file. The boolean parameter <with/without PIP> is used to select an appropriate file for serialization.
These files are kept in same location where the script is run. 
3. The number of operations actually executed may be more than specified in operationDetails.properties. This is because total number of operations 
are spread over operation types proportinate to the % specified. If any such calculation results in fractional value, it is rounded off to nearest 
max integer value.
4. The script attemps to perform a clean up activity which cleans up any VMs created for running benchmark utility. The delete vm operation may fail
for some VMs; in that case they have to be removed from inventory manually.

GENERATING GRAPH
------------------
./generate-report.sh 

The scipt takes no inputs.
It renders a Dual Axis graph on JFrame window.
