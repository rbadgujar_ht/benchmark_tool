#!/bin/bash
echo "Generating Graph Report"

how_to() {
        echo "Usage:"
        echo "./generate-report.sh"
}

java -classpath lib/aopalliance-1.0.jar:lib/commons-logging-1.1.1.jar:lib/dom4j-1.6.1.jar:lib/log4j-1.2.17.jar:lib/spring-aop-3.2.3.RELEASE.jar:lib/spring-beans-3.2.3.RELEASE.jar:lib/spring-context-3.2.3.RELEASE.jar:lib/spring-core-3.2.3.RELEASE.jar:lib/spring-expression-3.2.3.RELEASE.jar:lib/spring-tx-3.2.3.RELEASE.jar:lib/vijava-5.5.0.jar:Performance-1.1.1.jar:lib/jfreechart-1.0.19.jar:lib/jcommon-1.0.23.jar:lib/jasperreports-5.6.0.jar:lib/commons-digester-2.1.jar:lib/commons-collections-3.2.1.jar:lib/commons-beanutils-1.8.0.jar:lib/iText-2.1.7.js2.jar:. com.hytrust.performance.benchmark.PerfBenchmarkGraphNew

xdg-open test_report.pdf 
