# Copyright (c) 2014 HyTrust Inc. All rights reserved. 
#!/bin/bash
echo "Running Benchmark"

how_to() {
        echo "Usage:"
        echo "./run-benchmark.sh <output-file-name> <with/without-pip> <write-detailed-operation-results> <files-to-write-detailed-operation-result>"
        echo "	output-file-name : It is the file where results of performance benchmark run will be written"
        echo "	with/without-pip : (true/false) Boolean value specifying whether current run of performance benchmark is through HTCC or without HTCC."
	echo "	write-detailed-operation-results : (true/false) Boolean value to specify whether to write all operations detailed result to an output file."
	echo "	files-to-write-detailed-operation-result : Filename to write all operations detailed results."
}

if [ $# -le 3 ]; then
	if [ $# -eq 3 ]; then
		if [ $3 = 'true' ]; then
			how_to
			exit;
		fi
	
	else 
		how_to
		exit;
	fi
fi

java -classpath lib/aopalliance-1.0.jar:lib/commons-logging-1.1.1.jar:lib/dom4j-1.6.1.jar:lib/log4j-1.2.17.jar:lib/spring-aop-3.2.3.RELEASE.jar:lib/spring-beans-3.2.3.RELEASE.jar:lib/spring-context-3.2.3.RELEASE.jar:lib/spring-core-3.2.3.RELEASE.jar:lib/spring-expression-3.2.3.RELEASE.jar:lib/spring-tx-3.2.3.RELEASE.jar:lib/vijava-5.5.0.jar:lib/Performance-1.1.1.jar:. com.hytrust.performance.benchmark.PerfBenchmark $1 $2 $3 $4


