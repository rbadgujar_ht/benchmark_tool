package com.hytrust.performance.benchmark;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

@Service
public class A {
		
	@PostConstruct
	public void initA() {
		System.out.println("A Post Construct started");
		
		try {
			Thread.sleep(10 * 1000);
		} catch (Exception e) {
		}
		
		System.out.println("A Post Construct finished");
	}
	
	@PreDestroy
	public void destroy(){
		System.out.println("A Destroy started");
		try {
			Thread.sleep(10 * 1000);
		} catch (Exception e) {
			
		}
		System.out.println("A destroy finished");
	}

}
