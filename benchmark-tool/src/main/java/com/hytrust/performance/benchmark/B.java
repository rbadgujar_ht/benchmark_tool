package com.hytrust.performance.benchmark;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

@Service
public class B {
	

	@Resource
	private A a;
	
	@PostConstruct
	public void initB() {
		System.out.println("B Post Construct started");
		
		try {
			Thread.sleep(20 * 1000);
		} catch (Exception e) {
		}
		
		System.out.println("B Post Construct finished");
	}
	
	@PreDestroy
	public void destroyB(){
		System.out.println("B Destroy started");
		try {
			Thread.sleep(10 * 1000);
		} catch (Exception e) {
			
		}
		System.out.println("B destroy finished");
	}

}
