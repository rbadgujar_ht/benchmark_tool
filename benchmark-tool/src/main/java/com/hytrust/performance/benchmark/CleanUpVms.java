/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark;

import java.rmi.RemoteException;
import java.util.List;

import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.ops.sdkimpl.VCenterHelper;
import com.hytrust.performance.benchmark.properties.ConnectionDetails;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.vmware.vim25.VirtualMachinePowerState;
import com.vmware.vim25.mo.Folder;
import com.vmware.vim25.mo.InventoryNavigator;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.VirtualMachine;

public class CleanUpVms {
	
	public static void main(String[] args) {
		
		ConnectionDetails conDetails = (ConnectionDetails)SpringInitializer.getApplicationContext().getBean(ConnectionDetails.class);
		VCenterHelper vcHelper = (VCenterHelper)SpringInitializer.getApplicationContext().getBean(VCenterHelper.class);
		
		List<VCenterDetails> vcs = conDetails.getvCenters();
		for (VCenterDetails vc : vcs) {
			ServiceInstance si = vcHelper.login(vc.getUrl(), vc.getUsername(), vc.getPassword());
			findAndDeleteVMs(si);
		}
	}
	
	
	//Find out vm whose name starts with vm_perf_ and delete them. 
	public static void findAndDeleteVMs(ServiceInstance si) {
		Folder rootFolder = si.getRootFolder();
		try {
			ManagedEntity[] managedEntities = new InventoryNavigator(rootFolder).searchManagedEntities("VirtualMachine");
			
			for (ManagedEntity managedEntity : managedEntities) {
				VirtualMachine vm = (VirtualMachine)managedEntity;
				
				if(vm.getName().startsWith("vm_perf_htcc_")) {
					String name = vm.getName();
					System.out.println("VM found with name : "+name);
					
					VirtualMachinePowerState curState = vm.getRuntime().getPowerState();
					
					if(curState == VirtualMachinePowerState.poweredOn) {
						Task t = vm.powerOffVM_Task();
						t.waitForTask();
					}
					
					Task t = vm.destroy_Task();
					
					if(t.waitForTask() == Task.SUCCESS) {
						System.out.println("DELETE_VM successfull "+name);
					} else {
						System.out.println("DELETE_VM failed "+name);
					}	
				}
			}
			
		} catch (RemoteException | InterruptedException e) {			
			e.printStackTrace();
		}
	}
}
