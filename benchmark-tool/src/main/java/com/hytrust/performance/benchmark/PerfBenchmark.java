/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark;

import java.io.File;
import java.util.Map;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.PerfResult;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.init.ApplicationInitializer;
import com.hytrust.performance.benchmark.operations.AddHostOperation;
import com.hytrust.performance.benchmark.properties.OperationDetails;
import com.hytrust.performance.benchmark.result.ResultAggregator;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.hytrust.performance.benchmark.work.distribute.IOperationRandomizer;
import com.hytrust.performance.benchmark.work.distribute.OperationRandomizerImpl;
import com.hytrust.performance.benchmark.work.distribute.WorkDistributor;

public class PerfBenchmark {
	private static Logger logger = Logger.getLogger(PerfBenchmark.class);
	public static void main(String[] args) {
		
	    if(args.length <= 3) {
	        if(args.length >=3 && new Boolean(args[2])) {
	            if(args.length < 4) {
	            	System.out.println("Please provide a filename to write detailed operation results all the inputs.");
	                System.exit(0);
	            }
	        } else if (args.length >=3 && !new Boolean(args[2])){
	            //Its okay not to specify filename if detailed operation results are not written to a file 
	        } else {
	        	System.out.println("Please provide all the inputs.");
                System.exit(0);
	        }
	    }

		ApplicationInitializer appInit = SpringInitializer.getApplicationContext().getBean(ApplicationInitializer.class);
		
		/*Startup
		 * 1. Load sessions in session cache
		 * 2. Laods Vcenter Info. (Hosts info)
		 * 3. Create vms
		 * */		
		appInit.start();
		
		try {
			logger.info("Stopping for 60s");
			Thread.sleep(60*1000);
		} catch (Exception e) {
			
		}
		
		//Core Part
		WorkDistributor wd = SpringInitializer.getApplicationContext().getBean(WorkDistributor.class); 
		wd.distributeTasks();
		
		wd.shutDown();
		
		try{
			while (!wd.isTerminated()) {
				//Wait for executor termination
			}
		}catch(InterruptedException e) {
			System.out.println("Exception while waiting for executor to terminate.."+e.getMessage());
		}
		
		logger.info(ResultAggregator.getPerformanceResult().size());
		Map<OperationTypes, PerfResult> results = ResultAggregator.aggregate();
		
		PerformanceBenchmarkUtils.writePerfResults(new File(args[0]), results);
		String serFileName = "output";
		if(!new Boolean(args[1])) {
		    serFileName = serFileName + "-withoutPIP.ser";
		} else {
		    serFileName = serFileName + "-withPIP.ser";
		}
		PerformanceBenchmarkUtils.writeSerializedPerfResults(results, new File(serFileName));
		
		if(new Boolean(args[2])) {
		    PerformanceBenchmarkUtils.writeDetailedOpResults(ResultAggregator.getOperationResults(), new File(args[3]));
		}
		
		/*Finish
		 * 1. Deletes all vms created
		 * */
		appInit.stop();
		
	}

}
