/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark;


import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.springframework.core.io.ClassPathResource;

import com.hytrust.performance.benchmark.beans.PerfResult;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;

public class PerfBenchmarkGraph extends ApplicationFrame {

	private static final long serialVersionUID = 1L;

	
	public PerfBenchmarkGraph(final String title) throws IOException{

        super(title);

        String filename = "output";
    	
    	Map<OperationTypes, PerfResult> resultsWithPip = null;
    	Map<OperationTypes, PerfResult> resultsWithoutPip = null;
    	File pipFile = null;
    	File withoutPipFile = null;
    	try {
    		pipFile = new ClassPathResource(filename+"-withPIP.ser").getFile();
    		resultsWithPip = PerformanceBenchmarkUtils.readSerializedPerfResults(pipFile);
		} catch (Exception e) {
			System.out.println(" Exception in reading withPIP results map "+e.getMessage());
		}    
    	
        try {
        	withoutPipFile = new ClassPathResource(filename+"-withoutPIP.ser").getFile();
        	resultsWithoutPip = PerformanceBenchmarkUtils.readSerializedPerfResults(withoutPipFile);
		} catch (Exception e) {
			System.out.println(" Exception in reading withoutPIP results map "+e.getMessage());
		}
        
        final CategoryDataset dataset1 = createDataset1(resultsWithPip, resultsWithoutPip);

        // create the chart...
        final JFreeChart chart = ChartFactory.createBarChart(
            "Operation Results",        // chart title
            "Operation Type",               // domain axis label
            "Time Taken",                  // range axis label
            dataset1,                 // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips?
            false                     // URL generator?  Not required...
        );

        
        chart.setBackgroundPaint(Color.white);
        //chart.getLegend().setAnchor(Legend.SOUTH);

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF));
        plot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);

        final CategoryDataset dataset2 = createDataset2(resultsWithPip, resultsWithoutPip);
        plot.setDataset(1, dataset2);
        plot.mapDatasetToRangeAxis(1, 1);

        final CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
        
        final ValueAxis axis2 = new NumberAxis("No of Operations");
        plot.setRangeAxis(1, axis2);

        final LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
        renderer2.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
        plot.setRenderer(1, renderer2);
        plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
        

        ChartUtilities.saveChartAsJPEG(new File("results.jpg"), chart, 600, 400);
        
        // add the chart to a panel...
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);

        if(pipFile != null) {
        	PerformanceBenchmarkUtils.deleteResultsFile(pipFile);
        }
        
        if(withoutPipFile != null) {
        	PerformanceBenchmarkUtils.deleteResultsFile(withoutPipFile);
        }
    }
  
    /**
     * Creates a sample dataset.
     *
     * @return  The dataset.
     */
    private CategoryDataset createDataset1(Map<OperationTypes, PerfResult> resultsWithPip,Map<OperationTypes, PerfResult> resultsWithoutPip) {

        // row keys...
        final String series1 = "With HTCC";
        final String series2 = "Without HTCC";

        // create the dataset...
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        if(resultsWithoutPip != null) {
        	Set<OperationTypes> opTypes = resultsWithoutPip.keySet();
        	for (OperationTypes operationTypes : opTypes) {
        		dataset.addValue(((resultsWithoutPip.get(operationTypes).getAverageTime())/1000), series2, operationTypes.toString());
        	}
        }
        
        if(resultsWithPip != null) {
        	Set<OperationTypes> opTypes = resultsWithPip.keySet();
        	for (OperationTypes operationTypes : opTypes) {
        		dataset.addValue(((resultsWithPip.get(operationTypes).getAverageTime())/1000), series1, operationTypes.toString());
        	}
		}

        return dataset;

    }
    
    private CategoryDataset createDataset2(Map<OperationTypes, PerfResult> resultsWithPip,Map<OperationTypes, PerfResult> resultsWithoutPip) {

    	// create the dataset...
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    	
        // row keys...
        final String series1 = "No of Operations";

        boolean flag = false;
        if(resultsWithoutPip != null) {
        	Set<OperationTypes> opTypes = resultsWithoutPip.keySet();
        	for (OperationTypes operationTypes : opTypes) {
            	dataset.addValue(resultsWithoutPip.get(operationTypes).getTotalNumberOfOperations(), series1, operationTypes.toString());
            }
        	flag = true;
        }
        
        if(!flag && resultsWithPip != null) {
        	Set<OperationTypes> opTypes = resultsWithPip.keySet();
        	for (OperationTypes operationTypes : opTypes) {
            	dataset.addValue(resultsWithPip.get(operationTypes).getTotalNumberOfOperations(), series1, operationTypes.toString());
            }
        }

        return dataset;

    }

    public static void main(final String[] args) throws IOException {
    	
        final PerfBenchmarkGraph demo = new PerfBenchmarkGraph("Operation Results");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);

    }

}
