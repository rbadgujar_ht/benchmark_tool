package com.hytrust.performance.benchmark;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.core.io.ClassPathResource;

import com.hytrust.performance.benchmark.graph.PerfResultChartFactory;
import com.hytrust.performance.benchmark.graph.PerfResultGraph;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;

public class PerfBenchmarkGraphNew {
	
	
	public static void main(String[] args) {
		try {
			String sourceFile = new ClassPathResource("config/perftest.jrxml")
					.getFile().getAbsolutePath();

			String compliedName = JasperCompileManager.compileReportToFile(sourceFile);

			System.out.println(" Compiled report file name : " + compliedName);

			Collection<PerfResultGraph> dataList = PerfResultChartFactory.generateCollection();

			JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);

			Map parameters = new HashMap();

			String printFileName = JasperFillManager.fillReportToFile(compliedName, parameters, beanColDataSource);
			
			JasperExportManager.exportReportToPdfFile(printFileName, new ClassPathResource("/").getFile().getAbsolutePath()+"/test_report.pdf");
			
			System.out.println("Return to file...");
			
			File pipFile = null;
	    	File withoutPipFile = null;
	    
	    	String filename = "output";
	    	try {
	    		pipFile = new ClassPathResource(filename+"-withPIP.ser").getFile();
	    	}catch(IOException e) {	    		
	    	}
	    	try {
	    		withoutPipFile = new ClassPathResource(filename+"-withoutPIP.ser").getFile();
	    	}catch(IOException e) {	    		
	    	}
	    	
	    	if(pipFile != null) {
	        	PerformanceBenchmarkUtils.deleteResultsFile(pipFile);
	        }
	    	
	        if(withoutPipFile != null) {
	        	PerformanceBenchmarkUtils.deleteResultsFile(withoutPipFile);
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
