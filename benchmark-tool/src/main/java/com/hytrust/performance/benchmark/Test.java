package com.hytrust.performance.benchmark;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.init.ApplicationInitializer;
import com.hytrust.performance.benchmark.properties.ConnectionDetails;
import com.hytrust.performance.benchmark.util.SessionCache;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.hytrust.performance.benchmark.vmops.IVmOperations;
import com.hytrust.performance.benchmark.vmops.VCenterHelper;
import com.hytrust.performance.benchmark.vmops.VCenterHelperImpl;
import com.hytrust.performance.benchmark.vmops.VIJavaClient;
import com.vmware.vim25.mo.ServiceInstance;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext context = SpringInitializer.getApplicationContext();
		
		B a = (B) context.getBean(B.class);		
		
		((ClassPathXmlApplicationContext)context).registerShutdownHook();
		((ClassPathXmlApplicationContext)context).close();
		/*
		ApplicationInitializer appInit = SpringInitializer.getApplicationContext().getBean(ApplicationInitializer.class);
	
		VIJavaClient vmOps = (VIJavaClient)context.getBean(IVmOperations.class);
		
		ConnectionDetails conDetails = (ConnectionDetails)context.getBean(ConnectionDetails.class);
		
		VCenterHelper vcHelper = (VCenterHelperImpl)context.getBean(VCenterHelper.class);
		
		appInit.loadSessions();
		appInit.loadVCenterInfo();
		//appInit.startWaitForUpdateWorkers();
		
		List<VCenterDetails> vCenters = conDetails.getvCenters();
		
		VCenterDetails vc = vCenters.get(0);
		
		ServiceInstance si = SessionCache.getSI(vc.getUrl());
		
		String hostName = vc.getHosts().get(0).getHostName();
		String dcName = vc.getHosts().get(0).getDcName();
		String datastoreName = vc.getHosts().get(0).getDataStoreName();
		
		String vmName = args[0];
		
		vmOps.createVm(si, vmName, hostName, dcName, datastoreName);
		
		OpResult opResult = new OpResult();
		
		vmOps.getVm(si, vmName, opResult);
		
		//appInit.stopWaitForUpdatesWorkers();
		si.getServerConnection().logout();*/
	}

}
