/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.beans;

/**
 * @author rkonde
 * */

/**
 * Contains details of host in vCneter
 * 
 * */
public class HostDetails {
	
	private String hostName;
	
	private String dcName;
	
	private String dataStoreName;
	
	private String username;
	
	private String password;
	
	private String sslThumbPrint;

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getDcName() {
		return dcName;
	}

	public void setDcName(String dcName) {
		this.dcName = dcName;
	}

	public String getDataStoreName() {
		return dataStoreName;
	}

	public void setDataStoreName(String dataStoreName) {
		this.dataStoreName = dataStoreName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSslThumbPrint() {
		return sslThumbPrint;
	}

	public void setSslThumbPrint(String sslThumString) {
		this.sslThumbPrint = sslThumString;
	}
	
}
