/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.beans;


/**
 * @author rkonde
 * 
 * Contains Details of VM created
 * */
public class NodeDetails {
	
	private String vcUrl;
	
	private VCenterDetails vcDetails;
	
	private String vmName;
	
	private String hostName;

	public NodeDetails() {

	}

	public NodeDetails(String vcUrl, String vmName, VCenterDetails vcDetails) {
		this.vcUrl = vcUrl;
		this.vmName = vmName;
		this.vcDetails = vcDetails;
	}
	
	public NodeDetails(String vcUrl, VCenterDetails vcDetails) {
		this.vcUrl = vcUrl;
		this.vcDetails = vcDetails;
	}
	
	public String getVcUrl() {
		return vcUrl;
	}

	public void setVcUrl(String vcUrl) {
		this.vcUrl = vcUrl;
	}
		
	public VCenterDetails getVcDetails() {
		return vcDetails;
	}

	public void setVcDetails(VCenterDetails vcDetails) {
		this.vcDetails = vcDetails;
	}

	public String getVmName() {
		return vmName;
	}

	public void setVmName(String vmName) {
		this.vmName = vmName;
	}
	
	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	@Override
	public boolean equals(Object obj) {
	    if(obj != null && obj instanceof NodeDetails)
	        return this.vcUrl.equals(((NodeDetails)obj).vcUrl);
	    else 
	        return false;
	}
}
