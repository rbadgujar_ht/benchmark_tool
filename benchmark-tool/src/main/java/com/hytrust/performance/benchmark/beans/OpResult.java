/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.beans;

import java.util.Date;

import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;

/**
 * Bean emitted by each step performed within an operation.
 * @author rbadgujar
 *
 */
public class OpResult {

    private String node;
    
    private Date startTime = new Date();
    
    private Date endTime = new Date();
    
    private OperationTypes operationType;
    
    private OperationStatus operationStatus;
    
    private String message;

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public OperationTypes getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationTypes operationType) {
        this.operationType = operationType;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
