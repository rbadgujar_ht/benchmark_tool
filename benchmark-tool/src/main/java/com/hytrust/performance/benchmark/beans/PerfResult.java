/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.beans;

import java.io.Serializable;

import com.hytrust.performance.benchmark.enums.OperationTypes;

/**
 * Holds detailed performance results for an operation type.
 * @author rbadgujar
 *
 */
public class PerfResult implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private OperationTypes operationType;
    private Long totalTime;
    private Float averageTime;
    private Integer totalNumberOfOperations;
    
    public OperationTypes getOperationType() {
        return operationType;
    }
    public void setOperationType(OperationTypes operationType) {
        this.operationType = operationType;
    }
    public Long getTotalTime() {
        return totalTime;
    }
    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }
    public Float getAverageTime() {
        return averageTime;
    }
    public void setAverageTime(Float averageTime) {
        this.averageTime = averageTime;
    }
    public Integer getTotalNumberOfOperations() {
        return totalNumberOfOperations;
    }
    public void setTotalNumberOfOperations(Integer totalNumberOfOperations) {
        this.totalNumberOfOperations = totalNumberOfOperations;
    }
    
}
