/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.beans;

import java.util.List;

/**
 * @author rkonde
 * */

/**
 * Contains details of vCenter
 * */
public class VCenterDetails {
	
	private String url;
	
	private String username;
	
	private String password;
	
	private int noOfHosts;
	
	private List<HostDetails> hosts;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
		
	public int getNoOfHosts() {
		return noOfHosts;
	}

	public void setNoOfHosts(int noOfHosts) {
		this.noOfHosts = noOfHosts;
	}

	public List<HostDetails> getHosts() {
		return hosts;
	}

	public void setHosts(List<HostDetails> hosts) {
		this.hosts = hosts;
	}

	@Override
	public String toString() {
		return getUrl()+" "+getUsername()+" "+getPassword();
	}
}
