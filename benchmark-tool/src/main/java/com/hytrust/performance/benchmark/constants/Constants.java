/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.constants;

/**
 * @author rbadgujar
 * 
 * Contains Constants used to determine type of execution.
 * */
public class Constants {

    public static final String SEQ = "seq";
    public static final String PARALLEL = "parallel";
}
