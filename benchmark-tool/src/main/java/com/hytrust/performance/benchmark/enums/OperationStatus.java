/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.enums;

/**
 * @author rbadgujar
 * 
 * Status of Operation such as success , fail. Used in OpResults.
 * */
public enum OperationStatus {

    SUCESS, FAIL, ERROR, EXCEPTION;
}
