/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.enums;

/**
 * @author rbadgujar
 * 
 * Provides type of operations can be performed.
 * */
public enum OperationTypes {

    LOGIN(0), LOGOFF(1), CREATE_VM(2), DELETE_VM(3), SNAPSHOT_VM(4), POWER_ON_VM(5), POWER_OFF_VM(6), REBOOT_VM(7), GET_VM(8), MIGRATE_VM(9), CLONE_VM(10), ADD_HOST(11), REBOOT_HOST(12), DELETE_HOST(13), DISCONNECT_HOST(14), RECONNECT_HOST(15), CREATE_VIRTUAL_SWITCH(16), ADD_PORTGROUP(17),REMOVE_VIRTUAL_SWITCH(18), REMOVE_PORTGROUP(19), CREATE_CLUSTER(20), ADD_HOST_TO_CLUSTER(21), REMOVE_HOST_FROM_CLUSTER(22);
    
    private int opId;
    
    OperationTypes(int id) {
        this.opId = id;
    }
    
    public int getOpId() {
        return opId;
    }
    
    /**     
     * @param opId
     * @return
     */
    public static OperationTypes getOperationType(int opId) {
        OperationTypes operationType = null;
        switch (opId) {
        case 0:
            operationType = LOGIN;
            break;
        case 1:
            operationType = LOGOFF;
            break;
        case 2:
            operationType = CREATE_VM;
            break;
        case 3:
            operationType = DELETE_VM;
            break;
        case 4:
            operationType = SNAPSHOT_VM;
            break;
        case 5:
            operationType = POWER_ON_VM;
            break;
        case 6:
            operationType = POWER_OFF_VM;
            break;
        case 7:
            operationType = REBOOT_VM;
            break;
        case 8:
            operationType = GET_VM;
            break;
        case 9:
            operationType = MIGRATE_VM;
            break;    
        case 10:
        	operationType = CLONE_VM;
        	break;
        case 11:
        	operationType = ADD_HOST;
        	break;
        case 12:
        	operationType = REBOOT_HOST;
        	break;
        case 13:
        	operationType = DELETE_HOST;
        	break;
        case 14:
        	operationType = DISCONNECT_HOST;
        	break;
        case 15:
        	operationType = RECONNECT_HOST;
        	break;
        case 16:
        	operationType = CREATE_VIRTUAL_SWITCH;
        	break;
        case 17:
        	operationType = ADD_PORTGROUP;
        	break;
        case 18:
        	operationType = REMOVE_VIRTUAL_SWITCH;
        	break;
        case 19:
        	operationType = REMOVE_PORTGROUP;
        	break;
        case 20:
        	operationType = CREATE_CLUSTER;
        	break;
        case 21:
        	operationType = ADD_HOST_TO_CLUSTER;
        	break;
        case 22:
        	operationType = REMOVE_HOST_FROM_CLUSTER;
        	break;	
        	
        default:
            break;
        }
        return operationType;
    }
}
