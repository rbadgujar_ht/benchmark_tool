package com.hytrust.performance.benchmark.graph;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;

import net.sf.jasperreports.engine.JRAbstractChartCustomizer;
import net.sf.jasperreports.engine.JRChart;

public class ChartCustomizer extends JRAbstractChartCustomizer {

	public void customize(JFreeChart chart, JRChart jasperChart) {

		CategoryPlot categoryPlot = chart.getCategoryPlot();
		BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();
		
		// Spaces between bars
		renderer.setItemMargin(0.00);
	}
}