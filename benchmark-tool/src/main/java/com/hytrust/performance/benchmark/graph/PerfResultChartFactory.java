package com.hytrust.performance.benchmark.graph;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.core.io.ClassPathResource;

import com.hytrust.performance.benchmark.beans.PerfResult;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;

public class PerfResultChartFactory {
	
	public static Collection<PerfResultGraph> generateCollection() {
		
		List<PerfResultGraph> results = new ArrayList<PerfResultGraph>();
		
		String filename = "output";
		
		Map<OperationTypes, PerfResult> resultsWithPip = null;
    	Map<OperationTypes, PerfResult> resultsWithoutPip = null;
    	File pipFile = null;
    	File withoutPipFile = null;
    	try {
    		pipFile = new ClassPathResource(filename+"-withPIP.ser").getFile();
    		resultsWithPip = PerformanceBenchmarkUtils.readSerializedPerfResults(pipFile);
		} catch (Exception e) {
			System.out.println(" Exception in reading withPIP results map "+e.getMessage());
		}    
    	
        try {
        	withoutPipFile = new ClassPathResource(filename+"-withoutPIP.ser").getFile();
        	resultsWithoutPip = PerformanceBenchmarkUtils.readSerializedPerfResults(withoutPipFile);
		} catch (Exception e) {
			System.out.println(" Exception in reading withoutPIP results map "+e.getMessage());
		}
        
        if(resultsWithPip != null && resultsWithoutPip != null) {
        	Set<OperationTypes> opTypes = resultsWithoutPip.keySet();
        	for (OperationTypes opType : opTypes) {
        		
        		PerfResult pr1 = resultsWithoutPip.get(opType);
        		PerfResult pr2 = resultsWithPip.get(opType);
        		
        		PerfResultGraph pg = new PerfResultGraph();
        		pg.setOpType(opType);
        		pg.setAverageTimePip(pr2.getAverageTime());
        		pg.setAverageTimeNoPip(pr1.getAverageTime());
        		pg.setTotalNoOfOperations(pr1.getTotalNumberOfOperations());
        		
        		results.add(pg);
        	}
        	
        	return results;
        }
        
        if(resultsWithPip != null && resultsWithoutPip == null) {
        	Set<OperationTypes> opTypes = resultsWithPip.keySet();
        	for (OperationTypes opType : opTypes) {
        		
        		PerfResult pr2 = resultsWithPip.get(opType);
        		
        		PerfResultGraph pg = new PerfResultGraph();
        		pg.setOpType(opType);
        		pg.setAverageTimePip(pr2.getAverageTime());
        		pg.setAverageTimeNoPip(0);
        		pg.setTotalNoOfOperations(pr2.getTotalNumberOfOperations());
        		
        		results.add(pg);
        	}
        	
        	return results;
        }
        
        if(resultsWithPip == null && resultsWithoutPip != null) {
        	Set<OperationTypes> opTypes = resultsWithoutPip.keySet();
        	for (OperationTypes opType : opTypes) {
        		
        		PerfResult pr1 = resultsWithoutPip.get(opType);
        		
        		PerfResultGraph pg = new PerfResultGraph();
        		pg.setOpType(opType);
        		pg.setAverageTimePip(0);
        		pg.setAverageTimeNoPip(pr1.getAverageTime());
        		pg.setTotalNoOfOperations(pr1.getTotalNumberOfOperations());
        		
        		results.add(pg);
        	}
        	
        	return results;
        }
        	
		return results;
	}	

}
