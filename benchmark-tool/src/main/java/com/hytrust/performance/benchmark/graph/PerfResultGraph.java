package com.hytrust.performance.benchmark.graph;

import com.hytrust.performance.benchmark.enums.OperationTypes;

public class PerfResultGraph {
	private OperationTypes opType;
	
	private double averageTimePip;
	
	private double averageTimeNoPip;
	
	private int totalNoOfOperations;

	public OperationTypes getOpType() {
		return opType;
	}

	public void setOpType(OperationTypes opType) {
		this.opType = opType;
	}

	public double getAverageTimePip() {
		return averageTimePip;
	}

	public void setAverageTimePip(double averageTimePip) {
		this.averageTimePip = averageTimePip;
	}
	
	public double getAverageTimeNoPip() {
		return averageTimeNoPip;
	}
	
	public void setAverageTimeNoPip(double averageTimeNoPip) {
		this.averageTimeNoPip = averageTimeNoPip;
	}

	public int getTotalNoOfOperations() {
		return totalNoOfOperations;
	}

	public void setTotalNoOfOperations(int totalNoOfOperations) {
		this.totalNoOfOperations = totalNoOfOperations;
	}
	
	
}
