/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.init;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.ops.sdkimpl.IVmOperations;
import com.hytrust.performance.benchmark.ops.sdkimpl.VCenterHelper;
import com.hytrust.performance.benchmark.properties.ConnectionDetails;
import com.hytrust.performance.benchmark.properties.HostConfigs;
import com.hytrust.performance.benchmark.properties.OperationDetails;
import com.hytrust.performance.benchmark.util.SessionCache;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.ServiceInstance;

/**
 * @author rkonde
 * 
 * This class provides method to perform startup and tear down operations
 * */
@Service
public class ApplicationInitializer {
	
	private Logger logger = Logger.getLogger(ApplicationInitializer.class);
	private ExecutorService service = Executors.newCachedThreadPool();
	private ExecutorService serviceWFU = Executors.newCachedThreadPool();
	private List<Future<String>> futureWFU =new ArrayList<Future<String>>();
	
	@Autowired
	private ConnectionDetails conDetails;
	
	@Autowired
	private VCenterHelper vcHelper;
	
	@Autowired
	private OperationDetails opDetails;
	
	@Autowired
	private IVmOperations vmOps;
	
	@Autowired
	private HostConfigs hostConfigs;
	
	public void start() {
		loadSessions();
		loadVCenterInfo();
		createVms();		
		populateHosts();
				
	}
	
	public void stop() {
		deleteVms();
	}
	
	/**
	 * Creates Session for each VC and add to SessionCache 
	 */
	public void loadSessions() {
		logger.info("Started loading sessions into cache");
		
		List<VCenterDetails> vcList = conDetails.getvCenters();
		
		for (VCenterDetails vc : vcList) {
			ServiceInstance si = vcHelper.login(vc.getUrl(), vc.getUsername(), vc.getPassword());
			if(si == null) {
				logger.error("Unable to login into vc "+vc.getUrl()+" Exiting");
				//TODO : Instead of exiting skip this vc and make appropriate changes, if only one VC exit
				System.exit(0);
			}
			SessionCache.add(vc.getUrl(), si);
		}
		logger.info("Finished loading sessions into cache");
	}
	
	
	/**
	 * Load HostDetils for every VC 
	 */
	public void loadVCenterInfo(){
		logger.info("Started loading vCenter information");
		
		List<VCenterDetails> vcList = conDetails.getvCenters();
		try{
			for (VCenterDetails vCenterDetails : vcList) {
				ServiceInstance si = SessionCache.getSI(vCenterDetails.getUrl());				        
				vCenterDetails.setHosts(vcHelper.getHostSystems(si));
				if(vCenterDetails.getHosts().size() == 0 ) {
					//TODO : Instead of exiting skip this vc and make appropriate changes, if only one VC exit
					logger.error("No host information available...Exiting");
					System.exit(0);
				}
				vCenterDetails.setNoOfHosts(vCenterDetails.getHosts().size());
			}			
		}catch(Exception e) {			
			logger.error("Exception in loading vCenter information. "+e.getMessage());
			e.printStackTrace();
		}		
		logger.info("Finished loading vCenter information");
	}
	
	
	/**
	 * Starts Worker to create VMs in every VC, waits till all workers finish.
	 */
	public void createVms() {
		logger.info("Started Creating VMs.");
		
		String execType = opDetails.getExecutionType();
		
		/* TODO : Create only required number of vms i.e. equal to the number of delete operations specified.
		 * Use this strategy only when CREATE_VM operations are not specified whereas DELETE_VM operations are 
		 * specified.*/
		int opsCreate = opDetails.getMapOfConfiguredOpsNoOfOps().get(OperationTypes.CREATE_VM) + 
				opDetails.getMapOfConfiguredOpsNoOfOps().get(OperationTypes.CLONE_VM);
		int remOps = opDetails.getTotalNoOfOperation() - opsCreate;
		int noOfVCenter = conDetails.getNoOfVcenters();
		
		int vmsPerVc = remOps/noOfVCenter;
		
		List<VCenterDetails> vcs = conDetails.getvCenters();
		
		List<Future<String>> f = new ArrayList<Future<String>>();
		
		//Submit Threads 
		for (VCenterDetails vCenterDetails : vcs) {
			f.add(service.submit(new CreateVmWorker(vmsPerVc, vCenterDetails, execType)));
		}
		
		//Wait for threads to Complete
		for (Future<String> future : f) {
			try {
				future.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		service.shutdown();
		logger.info("Finished Creating VMs.");
	}
	
	/**
	 * Starts waitForUpdates() background threads.
	 * 
	 * Note : Not used currently.
	 * */
	public void startWaitForUpdateWorkers() {

		ConnectionDetails conDetails = SpringInitializer.getApplicationContext().getBean(ConnectionDetails.class);
		
		List<VCenterDetails> vcDetails = conDetails.getvCenters();
		
		
		for (VCenterDetails vc : vcDetails) {
			ServiceInstance si = SessionCache.getSI(vc.getUrl());
			
			WaitForUpdatesWorker worker = new WaitForUpdatesWorker(si);
			
			Future<String> f = serviceWFU.submit(worker);
			futureWFU.add(f);
		}
		
	}
	
	/**
	 * Starts waitForUpdates() threads.
	 * 
	 * Note : Not used currently.
	 * */
	public void stopWaitForUpdatesWorkers() {
		
		serviceWFU.shutdown();
		
		for (Future<String> future : futureWFU) {
			future.cancel(true);
		}
		
	}
	
	
	/**
	 * Deletes all remaining VMs created during startup 
	 * TODO : Delets VM in sequence. Can be made multithreaded.
	 */
	public void deleteVms() {
		logger.info("Started deleting VMs");
		
		String execType = opDetails.getExecutionType();

		loadSessions(); //Just to make sure sessions are valid.		
		
		if(execType.equals("seq")) {
			deleteVmsSeq();
		} else {
			deleteVmsParallel();
		}
		logger.info("Finished deleting VMs");
	}
	
	public void deleteVmsSeq() {
		
		List<VCenterDetails> vcs = conDetails.getvCenters();
		for (VCenterDetails vc : vcs) {
			ServiceInstance si = SessionCache.getSI(vc.getUrl()); 
		
			int size = VMContainerAccessor.getSize(vc.getUrl());
			while (size>0) {
				NodeDetails vmDetails = VMContainerAccessor.getVm(vc.getUrl());
				vmOps.deleteVm(si, vmDetails.getVmName());
				size = VMContainerAccessor.getSize(vc.getUrl());
			}	
			//Delete removed vms
			int count = VMContainerAccessor.getRemovedVmsListSize(vc.getUrl());		
			for (int i=0; i < count; i++)
			{
				NodeDetails nodeDetails = VMContainerAccessor.getFromRemovedVmsList(vc.getUrl());
				vmOps.deleteVm(si, nodeDetails.getVmName());
			}
		}	
		
	}
	
	public void deleteVmsParallel() {
		int size = VMContainerAccessor.getSize();
		
		while (size>0) {
			NodeDetails vmDetails = VMContainerAccessor.getVm();
		
			ServiceInstance si = SessionCache.getSI(vmDetails.getVcUrl()); 
		
			vmOps.deleteVm(si, vmDetails.getVmName());
			size = VMContainerAccessor.getSize();
		}
		
		//Delete removed vms
		int count = VMContainerAccessor.getRemovedVmsListSize();		
		for (int i=0; i < count; i++)
		{
			NodeDetails nodeDetails = VMContainerAccessor.getFromRemovedVmsList();
			ServiceInstance si = SessionCache.getSI(nodeDetails.getVcUrl()); 			
			vmOps.deleteVm(si, nodeDetails.getVmName());
		}
	}

	public void populateHosts() {
		logger.info("Started populating host information");
		List<VCenterDetails> vcDetails = conDetails.getvCenters();

		List<HostDetails> hostList = hostConfigs.getHosts();

		for (HostDetails hostDetails : hostList) {
			boolean isAlreadyInVc = false;
			for (VCenterDetails vc : vcDetails) {
				ServiceInstance si = SessionCache.getSI(vc.getUrl());

				HostSystem host;
				try {
					host = vcHelper.getHostSystemByName(si,
							hostDetails.getHostName());
					if (host != null) {
						HostContainerAccessor.addVcHost(hostDetails);
						isAlreadyInVc = true;
						break;
					}
				} catch (RemoteException e) {
					logger.debug("Exception occured : " + e.getMessage());
				}

			}
			if (!isAlreadyInVc) {
				HostContainerAccessor.addNonVcHost(hostDetails);
			}
		}
		logger.info("Finished populating host information");
	}
}
