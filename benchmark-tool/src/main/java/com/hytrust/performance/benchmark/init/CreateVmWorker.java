/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.init;

import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.context.ApplicationContext;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.hytrust.performance.benchmark.constants.Constants;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.ops.sdkimpl.IVmOperations;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;
import com.hytrust.performance.benchmark.util.SessionCache;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.vmware.vim25.mo.ServiceInstance;

/**
 * @author rkonde
 * Worker to createVms on startup
 * If ExecutionType is parallel add VM to common queue.
 * Else add VMs to separate queue per user
 * */
public class CreateVmWorker implements Callable<String>{

	private int vmsPerVc;
	private VCenterDetails vCDetails;
	private String execType;
	
	public CreateVmWorker() {
		
	}
    
	public CreateVmWorker(int vmsPerVc, VCenterDetails vCDetails, String execType) {
		this.vmsPerVc = vmsPerVc;
		this.vCDetails = vCDetails;
		this.execType = execType;
	}
	
	@Override
	public String call() throws Exception {
		ApplicationContext context = SpringInitializer.getApplicationContext();
		IVmOperations vmOps = (IVmOperations)context.getBean(IVmOperations.class);		
		
		String url = vCDetails.getUrl();
		
		ServiceInstance si = SessionCache.getSI(url); 
		
		int noOfHost = vCDetails.getNoOfHosts();		
		int vmPerHost = vmsPerVc/noOfHost;
		List<HostDetails> hosts = vCDetails.getHosts();
		
		int diff = vmsPerVc%noOfHost;
        
        for (HostDetails hostDetails : hosts) {
            int vmToCreate = vmPerHost;
            if(diff > 0) {
                vmToCreate++;       //Add one extra VM
                diff--;             //Decrease difference
            }
            for (int i = 0; i < vmToCreate; i++) {
                String vmName = PerformanceBenchmarkUtils.getVmName();
                List<OpResult> results = vmOps.createVm(si, vmName, hostDetails.getHostName(), hostDetails.getDcName(), 
                        hostDetails.getDataStoreName());
                //Add VM to container only if it is created Successfully
                if (results.get(0).getOperationStatus() == OperationStatus.SUCESS) {
                    NodeDetails vmDetails = new NodeDetails(url, vmName, vCDetails);
                    vmDetails.setHostName(hostDetails.getHostName());
                    if(execType.equals(Constants.SEQ)) {
                        VMContainerAccessor.add(url, vmDetails);              
                    } else {
                        VMContainerAccessor.add(vmDetails);
                    }
                }               
            }
        }
		
		return "SUCCESS";
	}
	
}
