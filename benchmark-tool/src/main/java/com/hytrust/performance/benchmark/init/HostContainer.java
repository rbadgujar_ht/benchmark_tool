/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.init;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.springframework.stereotype.Service;
import com.hytrust.performance.benchmark.beans.HostDetails;
/**
 * @author ttapkir
 * 
 * Container to hold HostDetails provided to Operation
 * */
@Service
public class HostContainer {
	
	private BlockingQueue<HostDetails> vcHosts = new LinkedBlockingQueue<HostDetails>();
	private BlockingQueue<HostDetails> nonVcHosts = new LinkedBlockingQueue<HostDetails>();

	public BlockingQueue<HostDetails> getVcHosts() {
		return vcHosts;
	}
	
	public int getVcHostsSize() {
		return vcHosts.size();
	}
	
	public BlockingQueue<HostDetails> getNonVcHosts() {
		return nonVcHosts;
	}
	
	public int getNonVcHostsSize() {
		return nonVcHosts.size();
	}
}
