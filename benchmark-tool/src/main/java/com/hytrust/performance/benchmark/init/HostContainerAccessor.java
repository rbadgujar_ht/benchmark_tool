/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.init;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;
import com.hytrust.performance.benchmark.beans.HostDetails;

/**
 * @author ttapkir
 * 
 * Accessor to get and add HostDetails to HostContainer
 * */

public class HostContainerAccessor {

	private static Logger logger = Logger.getLogger(HostContainerAccessor.class);
	
    private static HostContainer container = new HostContainer();        
   
    private static HostContainer nonVcHostcontainer = new HostContainer();  
    
    private static List<String> hostsForVmOps = Collections.synchronizedList(new ArrayList<String>());
    
    public static void addVcHost(HostDetails hostDetails) {
    	logger.info("In Method add(hostDetails) "+hostDetails.getHostName());
        try {
            container.getVcHosts().put(hostDetails);
        } catch (InterruptedException e) {
        	logger.debug("Exception occured : "+e.getMessage());
        }
    }
   
    public static HostDetails getVcHost() {
    	logger.info("In method getVcHost()");
		try {
			return container.getVcHosts().take();
		} catch (InterruptedException e) {
			logger.debug("Exception occured : "+e.getMessage());
        }
        return null;
    }    
  
    public static int getVcHostsSize() {
    	return container.getVcHostsSize();
    }
    
    public static void addNonVcHost(HostDetails hostDetails) {
    	logger.info("In method addNonVcHost(hostDetails) "+hostDetails.getHostName());
        try {
        	nonVcHostcontainer.getNonVcHosts().put(hostDetails);
        } catch (InterruptedException e) {
        	logger.debug("Exception occured : "+e.getMessage());
        }        
    }
    
    public static HostDetails getNonVcHost() {
    	logger.info("In method getNonVcHost()");
		try {
			return nonVcHostcontainer.getNonVcHosts().take();
		} catch (InterruptedException e) {
			logger.debug("Exception occured : "+e.getMessage());
        }
        return null;
    }
   
    public static int getNonVcHostsSize() {
    	return nonVcHostcontainer.getNonVcHostsSize();
    }   
    
    public static void addToHostsForVmOps(String hostName)
    {
    	hostsForVmOps.add(hostName);
    }
    
    public static void removeFromHostsForVmOps(String hostName)
    {
    	hostsForVmOps.remove(hostName);
    }
    
    public static boolean isInHostsForVmOpsList(String hostName)
    {
    	return hostsForVmOps.contains(hostName);
    }
}
