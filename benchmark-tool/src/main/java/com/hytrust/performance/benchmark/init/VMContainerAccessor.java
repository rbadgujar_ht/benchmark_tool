/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.init;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.vmware.vim25.mo.ServiceInstance;

/**
 * @author rbadgujar
 * 
 * Accessor to get and add VmDetails to VmContainer
 * */

public class VMContainerAccessor {

	private static Logger logger = Logger.getLogger(VMContainerAccessor.class);
	
    private static VmContainer container = new VmContainer();
        
    private static Map<String, VmContainer> seqMap = new HashMap<String, VmContainer>();
    
    private static VmContainer removedVms = new VmContainer();
    private static Map<String, VmContainer> seqMapRemovedVms = new HashMap<String, VmContainer>();
    
    public static void add(NodeDetails vmDetails) {
    	logger.info("In method add(VmDetails) "+vmDetails.getVmName());
        try {
            container.getVms().put(vmDetails);
        } catch (InterruptedException e) {
        	logger.debug("Exception occured : "+e.getMessage());
        }
    }

    public static void add(String vcUrl, NodeDetails vmDetails) {
    	logger.info("In method add(vcUrl,VmDetails) "+vcUrl+" "+vmDetails.getVmName());
    	try {
			VmContainer vmContainer = seqMap.get(vcUrl);
			if(vmContainer == null) {
				vmContainer = new VmContainer();
				seqMap.put(vcUrl, vmContainer);
			}
			vmContainer.getVms().put(vmDetails);
		} catch (Exception e) {
			logger.debug("Exception occured : "+e.getMessage());
		}    
    }
    
    
    public static NodeDetails getVm() {
    	logger.info("In method getVm()");
        try {
            if(container.getSize() > 0) {
            	logger.info("Exiting getVm()");
                return container.getVms().take();
            }
        } catch (InterruptedException e) {
        	logger.debug("Exception occured : "+e.getMessage());
        }
        return null;
    }
    
    public static NodeDetails getVm(String vcUrl) {
    	logger.info("In method getVm(vcUrl) "+vcUrl);
    	try {
			VmContainer vmContainer = seqMap.get(vcUrl);
			if(vmContainer == null) {
				return null;
			}
			return vmContainer.getVms().take();
		} catch (Exception e) {
			logger.debug("Exception occured : "+e.getMessage());
		}    
        return null;
    }  
    
    public static int getSize() {
    	return container.getSize();
    }
    
    public static int getSize(String vcUrl) {
    	VmContainer vmContainer = seqMap.get(vcUrl);
		if(vmContainer == null) {
			return 0;
		}
		return vmContainer.getSize();
    }
    
    
    public static void addToRemovedVmsList(NodeDetails vmDetails) {
    	logger.info("In method addToRemovedVmsList(vmDetails) "+vmDetails.getVmName());
        try {
        	removedVms.getVms().put(vmDetails);
        } catch (InterruptedException e) {
        	logger.debug("Exception occured : "+e.getMessage());
        }
    }
   
  
    public static NodeDetails getFromRemovedVmsList() {
    	logger.info("In method getFromRemovedVmsList()");
        try {
            if(removedVms.getSize() > 0) {
            	logger.info("Exiting getVm()");
                return removedVms.getVms().take();
            }
        } catch (InterruptedException e) {
        	logger.debug("Exception occured : "+e.getMessage());
        }
        return null;
    }
    public static int getRemovedVmsListSize() {
    	return removedVms.getSize();
    }
    
    
    public static void addToRemovedVmsList(String vcUrl, NodeDetails vmDetails) {
    	logger.info("In method addToRemovedVmsList(vcUrl,VmDetails) "+vcUrl+" "+vmDetails.getVmName());
    	try {
			VmContainer vmContainer = seqMapRemovedVms.get(vcUrl);
			if(vmContainer == null) {
				vmContainer = new VmContainer();
				seqMapRemovedVms.put(vcUrl, vmContainer);
			}
			vmContainer.getVms().put(vmDetails);
		} catch (Exception e) {
			logger.debug("Exception occured : "+e.getMessage());
		}    
    }
    
    public static NodeDetails getFromRemovedVmsList(String vcUrl) {
    	logger.info("In method getFromRemovedVmsList(vcUrl) "+vcUrl);
    	try {
			VmContainer vmContainer = seqMapRemovedVms.get(vcUrl);
			if(vmContainer == null) {
				return null;
			}
			return vmContainer.getVms().take();
		} catch (Exception e) {
			logger.debug("Exception occured : "+e.getMessage());
		}    
        return null;
    }  
    
    public static int getRemovedVmsListSize(String vcUrl) {
    	VmContainer vmContainer = seqMapRemovedVms.get(vcUrl);
		if(vmContainer == null) {
			return 0;
		}
		return vmContainer.getSize();
    }
}
