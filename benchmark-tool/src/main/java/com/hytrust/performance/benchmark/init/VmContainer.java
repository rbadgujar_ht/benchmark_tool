/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.init;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.beans.NodeDetails;

/**
 * @author rbadgujar
 * 
 * Container to hold VmDetails provided to Operation
 * */
@Service
public class VmContainer {
	
	private BlockingQueue<NodeDetails> vms = new LinkedBlockingQueue<NodeDetails>();

	public BlockingQueue<NodeDetails> getVms() {
		return vms;
	}
	
	public int getSize() {
		return vms.size();
	}
}
