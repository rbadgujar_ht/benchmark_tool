/*
 * Copyright (c) 2015 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.init;

import java.util.concurrent.Callable;

import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.UpdateSet;
import com.vmware.vim25.WaitOptions;
import com.vmware.vim25.mo.InventoryView;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.PropertyFilter;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.View;
import com.vmware.vim25.mo.ViewManager;

/*
 * This class is not currently used. Can be used as background thread for waitForUpdates() calls.
 * */
public class WaitForUpdatesWorker implements Callable<String> {
		
	private ServiceInstance si = null;
	
	private boolean stop = true;
	
	public WaitForUpdatesWorker(ServiceInstance  si) {
		this.si = si;
	}
	
	@Override
	public String call() throws Exception {
				
		
		ViewManager viewMng = si.getViewManager();
		
		InventoryView invView = viewMng.createInventoryView();
		
		PropertyCollector pc = si.getPropertyCollector();
		
		PropertyFilterSpec pFSpec = new PropertyFilterSpec();
		
		ObjectSpec objSpec = createObjectSpec(invView);
		
		String type1 = "ManagedEntity";		
		String props1 [] = {"name","parent","overallStatus","tag","effectiveRole","disabledMethod"};		
		PropertySpec ps1 = createPropertySpec(type1, props1);
		
		String type2 = "Folder";
		String props2 [] = {"childType","childEntity.length"};		
		PropertySpec ps2 = createPropertySpec(type2, props2);		
		
		String type3 = "ComputeResource";
		String props3 [] = {"host.length"};		
		PropertySpec ps3 = createPropertySpec(type3, props3);
		
		String type4 = "StoragePod";
		String props4 [] = {"summary.capacity", "summary.freeSpace", "podStorageDrsEntry.storageDrsConfig.podConfig.enabled"};		
		PropertySpec ps4 = createPropertySpec(type4, props4);
		
		String type5 = "VirtualApp";
		String props5 [] = {"parentFolder", "parentVApp", "summary", "vAppConfig.managedBy"};		
		PropertySpec ps5 = createPropertySpec(type5, props5);
		
		String type6 = "ResourcePool";
		String props6 [] = {"resourcePool.length", "vm.length"};		
		PropertySpec ps6 = createPropertySpec(type6, props6);
		
		String type7 = "DistributedVirtualSwitch";
		String props7 [] = {"config.uuid", "capability", "config.uplinkPortgroup", "config.numPorts"};		
		PropertySpec ps7 = createPropertySpec(type7, props7);
		
		String type8 = "DistributedVirtualPortgroup";
		String props8 [] = {"config.distributedVirtualSwitch", "config.key", "config.numPorts"};		
		PropertySpec ps8 = createPropertySpec(type8, props8);
		
		String type9 = "HostSystem";
		String props9 [] = {"summary.runtime.connectionState", "summary.runtime.powerState", "summary.config.vmotionEnabled", "capability", "summary.runtime.inMaintenanceMode", 
							"summary.config.product", "configIssue"};		
		PropertySpec ps9 = createPropertySpec(type9, props9);
				
		String type10 = "VirtualMachine";
		String props10 [] = {"resourcePool", "runtime", "summary.config.template", "capability.multipleSnapshotsSupported", "summary.config.ftInfo", "parentVApp", 
							"capability.recordReplaySupported", "config.managedBy", "config.ftInfo", "summary.runtime.faultToleranceState"};
		PropertySpec ps10 = createPropertySpec(type10, props10);
				
		String type11 = "VirtualApp";
		String props11 [] = {"summary"};				
		PropertySpec ps11 = createPropertySpec(type11, props11);
		
		String type12 = "Datastore";
		String props12 [] = {"summary.accessible", "summary.type", "summary.maintenanceMode", "summary.capacity", "summary.freeSpace", "summary.uncommitted", 
								"summary.multipleHostAccess", "capability.perFileThinProvisioningSupported", "host", "info"	};				
		PropertySpec ps12 = createPropertySpec(type12, props12);
		
		String type13 = "ClusterComputeResource";
		String props13 [] = {"configuration.drsConfig", "configuration.dasConfig"};				
		PropertySpec ps13 = createPropertySpec(type13, props13);
		
		pFSpec.setObjectSet(new ObjectSpec[]{ objSpec });
		pFSpec.setPropSet(new PropertySpec[]{ ps1, ps2, ps3, ps4, ps5, ps6, ps7, ps8, ps9, ps10, ps11, ps12, ps13});
		
		PropertyFilter pf = pc.createFilter(pFSpec, false);
		
		String version = "";
		
		WaitOptions wait = new WaitOptions();
		wait.setMaxWaitSeconds(20*1000);
		
		while(stop) {
			UpdateSet upSet = pc.waitForUpdatesEx(version,wait);
			
			version = upSet.getVersion();
			
			System.out.println("Updates received....");
		}
		
		return "success";
	}
	
	public ObjectSpec createObjectSpec(View view) {
		
		ObjectSpec objSpec = new ObjectSpec();
		
		objSpec.setObj(view.getMOR());
		objSpec.setSkip(true);
		
		TraversalSpec tSpec = new TraversalSpec();
		tSpec.setType(view.getMOR().getType());
		tSpec.setPath("view");
		
		objSpec.setSelectSet(new SelectionSpec[]{tSpec});
		
		return objSpec;
	}
	
	public PropertySpec createPropertySpec(String type, String[] props) {
		
		PropertySpec pSpec = new PropertySpec();
		
		pSpec.setType(type);
		pSpec.setPathSet(props);
		pSpec.setAll(Boolean.TRUE);
		
		return pSpec;		
	}
	
	public void stop() {
		stop = false;
	}
}
