/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.operations;

import java.util.Date;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.ops.sdkimpl.IOperations;
import com.hytrust.performance.benchmark.ops.sdkimpl.IVmOperations;
import com.hytrust.performance.benchmark.ops.sdkimpl.VCenterHelper;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.vmware.vim25.mo.ServiceInstance;

/**
 * @author rbadgujar
 *
 */
public abstract class AbstractOperation {

    protected IVmOperations vmOps;
    protected IOperations hostOps;
    protected NodeDetails vmDetails;
    protected ServiceInstance si;
    protected HostDetails hostDetails;
    
    public abstract void execute();
    
    public void setVMOperationsImpl(IVmOperations vmOperations) {
        this.vmOps = vmOperations;
    }
    public void setHostOpsImpl(IOperations hostOperations)
    {
    	this.hostOps = hostOperations;	
    }

    public NodeDetails getVmDetails() {
        return vmDetails;
    }

    public void setVmDetails(NodeDetails vmDetails) {
        this.vmDetails = vmDetails;
    }
    
    public void setSession(ServiceInstance serviceInstance) {
        this.si = serviceInstance;
    }
    
    public HostDetails getHostDetails() {
		return hostDetails;
	}

	public void setHostDetails(HostDetails hostDetails) {
		this.hostDetails = hostDetails;
	}

	public OpResult createSession() {    	
    	    VCenterHelper vcHelper = SpringInitializer.getApplicationContext().getBean(VCenterHelper.class);
            OpResult result = new OpResult();
            result.setNode(vmDetails.getVmName());
            result.setOperationType(OperationTypes.LOGIN);
            result.setStartTime(new Date());
            si = vcHelper.login(vmDetails.getVcUrl(), vmDetails.getVcDetails().getUsername(), vmDetails.getVcDetails().getPassword());
            result.setEndTime(new Date());
            if(si == null) {
            	result.setOperationStatus(OperationStatus.EXCEPTION);
            } else {
            	result.setOperationStatus(OperationStatus.SUCESS);
            }
           
    	return result;
    }
    
    public OpResult destroySession() {
    	OpResult result = new OpResult();
    	result.setNode(vmDetails.getVmName());
        result.setOperationType(OperationTypes.LOGOFF);
        result.setStartTime(new Date());
    	si.getServerConnection().logout();
    	result.setEndTime(new Date());
    	result.setOperationStatus(OperationStatus.SUCESS);
    	return result;
    }
}
