package com.hytrust.performance.benchmark.operations;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.result.ResultAggregator;

/**
 * @author ttapkir
 *
 * Provide method to perform add Host Operation using VI Java
 */
public class AddHostOperation extends AbstractOperation{
private Logger logger = Logger.getLogger(AddHostOperation.class);
	
	
    /** 
     * Adds standalone host 
     * If ServiceInstance is not present(Parallel Execution), login in to VCenter->Perform Operations and Logout
     * If ServiceInstance is present(Sequential Execution) , Perform Operation
     * Add OpResult for operations performed to list and adds list to ResultAggregator
     */
    @Override
    public void execute() {
        logger.info("Adding host ");
        // If SI is null, this is parallel exec. Do login and logoff in op.
        List<OpResult> results = new ArrayList<OpResult>();
        OpResult result = null;
        boolean parallel = false;
        
        if(this.si == null) {
            result = createSession();
            if(result.getOperationStatus() == OperationStatus.EXCEPTION) {
            	logger.info("Operation failed with exception "+result.getMessage());
            	return;
            }
        	results.add(result);
            parallel = true;
        }       
         
        VCenterDetails vcDetails = vmDetails.getVcDetails();
        HostDetails host = null;
        
       
        List<HostDetails> hosts = vcDetails.getHosts();
        Random r =new Random();
        host = hosts.get(r.nextInt(vcDetails.getNoOfHosts()));
        results.addAll(hostOps.addHost(si, host.getDcName()));
        
      
        if(parallel) {        	
			result = destroySession();
        	results.add(result);        	
        } 
        
        ResultAggregator.getPerformanceResult().add(results);
        logger.info("Finished adding host ");
    }
}
