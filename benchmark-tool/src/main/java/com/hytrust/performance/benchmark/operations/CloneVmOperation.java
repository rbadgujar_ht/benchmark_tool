package com.hytrust.performance.benchmark.operations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.ops.sdkimpl.VCenterHelper;
import com.hytrust.performance.benchmark.result.ResultAggregator;
import com.hytrust.performance.benchmark.util.SpringInitializer;

public class CloneVmOperation extends AbstractOperation {
	private Logger logger = Logger.getLogger(CloneVmOperation.class);
	public static int count = 0;
	
	@Override
	public void execute() {
		logger.info("Cloning VM "+vmDetails.getVmName());
        // If SI is null, this is parallel exec. Do login and logoff in op.
        List<OpResult> results = new ArrayList<OpResult>();
        OpResult result = null;
        boolean parallel = false;
        
    	if(this.si == null) {
            result = createSession();
            if(result.getOperationStatus() == OperationStatus.EXCEPTION) {
            	return;
            }
        	results.add(result);
            parallel = true;
        } 
    
        
        count++;
        String cloneName = vmDetails.getVmName()+"_clone_"+count;
        
        results.addAll(vmOps.cloneVm(si, vmDetails.getVmName(), cloneName));
        
        NodeDetails cloneVmDetails = new NodeDetails();
        
        cloneVmDetails.setVmName(cloneName);
        cloneVmDetails.setHostName(vmDetails.getHostName());
        cloneVmDetails.setVcDetails(vmDetails.getVcDetails());
        cloneVmDetails.setVcUrl(vmDetails.getVcUrl());
        
        // Logoff and add vmdetails back
        if(parallel) {
        	VMContainerAccessor.add(vmDetails);
        	if(results.get(2).getOperationStatus() == OperationStatus.SUCESS) {
        		VMContainerAccessor.add(cloneVmDetails);
        	}
        	
        	result = destroySession();
        	results.add(result);
        } else {
        	VMContainerAccessor.add(vmDetails.getVcUrl(), vmDetails);
        	if(results.get(1).getOperationStatus() == OperationStatus.SUCESS) {
        		VMContainerAccessor.add(cloneVmDetails.getVcUrl(),cloneVmDetails);
        	}        	
        }
        
        ResultAggregator.getPerformanceResult().add(results);
        logger.info("Finished Cloning VM "+vmDetails.getVmName());
	}
}
