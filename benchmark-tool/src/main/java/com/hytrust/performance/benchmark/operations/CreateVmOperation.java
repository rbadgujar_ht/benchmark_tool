/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.operations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.ops.sdkimpl.VCenterHelper;
import com.hytrust.performance.benchmark.result.ResultAggregator;
import com.hytrust.performance.benchmark.util.SpringInitializer;


/**
 * @author rkonde
 *
 * Provides method to perform Create VM Operation using VI Java
 * 
 */
public class CreateVmOperation extends AbstractOperation {

	private Logger logger = Logger.getLogger(CreateVmOperation.class);
	
	
    /** 
     * Creates VM
     * If ServiceInstance is not present(Parallel Execution), login in to VCenter->Perform Operations and Logout
     * If ServiceInstance is present(Sequential Execution) , Perform Operation
     * Add OpResult for operations performed to list and adds list to ResultAggregator
     */
    @Override
    public void execute() {
        logger.info("Creating VM "+vmDetails.getVmName());
        // If SI is null, this is parallel exec. Do login and logoff in op.
        List<OpResult> results = new ArrayList<OpResult>();
        OpResult result = null;
        boolean parallel = false;
        
        if(this.si == null) {
            result = createSession();
            if(result.getOperationStatus() == OperationStatus.EXCEPTION) {
            	return;
            }
        	results.add(result);
            parallel = true;
        }       
         
        VCenterDetails vcDetails = vmDetails.getVcDetails();
        HostDetails host = null;
        
        List<HostDetails> hosts = vcDetails.getHosts();
        //Will randomly pick Host. We can have more specific code to pick Host Separately
        Random r =new Random();
        host = hosts.get(r.nextInt(vcDetails.getNoOfHosts()));
        
        results.addAll(vmOps.createVm(si, vmDetails.getVmName(), host.getHostName(), host.getDcName(), host.getDataStoreName()));
        
        // Logoff and add vmdetails back
        if(parallel) {
        	//Add VM to container only if it is created Successfully
			if (results.get(1).getOperationStatus() == OperationStatus.SUCESS) {
				vmDetails.setHostName(host.getHostName());
				VMContainerAccessor.add(vmDetails);
			}
			
			result = destroySession();
        	results.add(result);        	
        } else {
        	if (results.get(0).getOperationStatus() == OperationStatus.SUCESS) {
        		vmDetails.setHostName(host.getHostName());
        		VMContainerAccessor.add(vmDetails.getVcUrl(), vmDetails);
        	}
        }
        
        ResultAggregator.getPerformanceResult().add(results);
        logger.info("Finished Creating VM "+vmDetails.getVmName());
    }
}
