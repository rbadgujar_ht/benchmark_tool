/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.operations;

import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.ops.sdkimpl.IOperations;
import com.hytrust.performance.benchmark.ops.sdkimpl.IVmOperations;
import com.hytrust.performance.benchmark.util.SpringInitializer;


/**
 * @author rbadgujar
 *
 * Factory to create operation objects
 */
public class OperationFactory {

    /**
     * Creates and returns object of operation for given operation type
     * @param operationType
     * @return
     */
    public static AbstractOperation getOperation(OperationTypes operationType) {
        AbstractOperation operation = null;
        IVmOperations vmOperations = (IVmOperations) SpringInitializer.getApplicationContext().getBean(IVmOperations.class);
        IOperations hostOperations = (IOperations) SpringInitializer.getApplicationContext().getBean(IOperations.class);
        switch (operationType) {
        case CREATE_VM:
            operation = new CreateVmOperation();
            break;
        case DELETE_VM:
            operation = new DeleteVmOperation();
            break;
        case POWER_ON_VM:
            operation = new PowerOnVmOperation();
            break;
        case POWER_OFF_VM:
            operation = new PowerOffVmOperation();
            break;
        case SNAPSHOT_VM:
            operation = new SnapshotVmOperation();
            break;
        case REBOOT_VM:
            operation = new RebootVmOperation();
            break;
        case MIGRATE_VM:
        	operation = new RelocateVmOperation();
        	break;
        case CLONE_VM:
        	operation = new CloneVmOperation();
        	break;
        case ADD_HOST:
        	operation = new AddHostOperation();
        	break;
        case REBOOT_HOST:
        	operation = new RebootHostOperation();
        	break; 
        case DELETE_HOST:
        	operation = new DeleteHostOperation();
        	break;
        case DISCONNECT_HOST:
        	operation = new DisconnectHostOperation();
        	break;
        case RECONNECT_HOST:
        	operation = new ReconnectHostOperation();
        	break;
        case CREATE_VIRTUAL_SWITCH:
        	operation = new AddVirtualSwitchOperation();
        	break;
        case ADD_PORTGROUP:
        	operation = new AddPortgroupOperation();
        	break;
        case REMOVE_VIRTUAL_SWITCH:
        	operation = new RemoveVirtualSwitchOperation();
        	break;
        case REMOVE_PORTGROUP:
        	operation = new RemovePortgroupOperation();
        	break;
        case CREATE_CLUSTER:
        	operation = new CreateClusterOperation();
        	break;
        case ADD_HOST_TO_CLUSTER:
        	operation = new AddHostToClusterOperation();
        	break;	
        case REMOVE_HOST_FROM_CLUSTER:
        	operation = new RemoveHostFromClusterOperation();
        	break;	        	
        }
        operation.setVMOperationsImpl(vmOperations);
        operation.setHostOpsImpl(hostOperations);
        return operation;
    }
    
}
