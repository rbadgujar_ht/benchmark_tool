package com.hytrust.performance.benchmark.operations;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.result.ResultAggregator;

/**
 * @author ttapkir
 *
 * Provides method to reboot host using VI JAVA
 */
public class RebootHostOperation extends AbstractOperation {

	private Logger logger = Logger.getLogger(RebootHostOperation.class);
	
	/** 
     * Reboot Host
     * If ServiceInstance is not present(Parallel Execution), login in to VCenter->Perform Operations and Logout
     * If ServiceInstance is present(Sequential Execution) , Perform Operation
     * Add OpResult for operations performed to list and adds list to ResultAggregator
     */
    @Override
    public void execute() {
       logger.info("Rebooting host "/*+hostDetails.getHostName()*/);
       // If SI is null, this is parallel exec. Do login and logoff in op.
       List<OpResult> results = new ArrayList<OpResult>();
       OpResult result =null;
       boolean parallel = false;
       
	   if(this.si == null) {
           result = createSession();
           if(result.getOperationStatus() == OperationStatus.EXCEPTION) {
        	logger.info("Operation failed with exception "+result.getMessage());
           	return;
           }
       	results.add(result);
           parallel = true;
       } 
   
       
       results.addAll(hostOps.rebootHost(si));
       // Logoff and add vmdetails back
       if(parallel) {
       	   result = destroySession();
       	   results.add(result);
       }
      
       ResultAggregator.getPerformanceResult().add(results);
       logger.info("Finished Reboot Host "/*+hostDetails.getHostName()*/);
    }

}