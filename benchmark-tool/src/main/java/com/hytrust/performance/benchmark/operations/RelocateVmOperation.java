package com.hytrust.performance.benchmark.operations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.ops.sdkimpl.VCenterHelper;
import com.hytrust.performance.benchmark.result.ResultAggregator;
import com.hytrust.performance.benchmark.util.SpringInitializer;

public class RelocateVmOperation extends AbstractOperation {
	private Logger logger = Logger.getLogger(MigrateVmOperation.class);
	
	@Override
	public void execute() {
		logger.info("Migrating VM "+vmDetails.getVmName());
        // If SI is null, this is parallel exec. Do login and logoff in op.
        List<OpResult> results = new ArrayList<OpResult>();
        OpResult result = null;
        boolean parallel = false;
        
    	if(this.si == null) {
            result = createSession();
            if(result.getOperationStatus() == OperationStatus.EXCEPTION) {
            	return;
            }
        	results.add(result);
            parallel = true;
        } 
    
         
        VCenterDetails vcDetails = vmDetails.getVcDetails();
        
        HostDetails host = null;
        
        List<HostDetails> hosts = vcDetails.getHosts();
        
        String currentHost = vmDetails.getHostName();
        
        //Will randomly pick Host. We can have more specific code to pick Host Separately
        Random r =new Random();
        
        boolean found = false;
        while (!found) {
        	host = hosts.get(r.nextInt(vcDetails.getNoOfHosts()));
        	if(!currentHost.equals(host.getHostName())) {
        		found = true;
        	}
		}
        
        results.addAll(vmOps.relocateVm(si, vmDetails.getVmName(), host.getDcName(), host.getHostName()));
        
        // Logoff and add vmdetails back
        if(parallel) {
        	VMContainerAccessor.add(vmDetails);
        	result = destroySession();
        	results.add(result);
        } else {
        	VMContainerAccessor.add(vmDetails.getVcUrl(), vmDetails);
        }
        
        ResultAggregator.getPerformanceResult().add(results);
        logger.info("Finished Migrating VM "+vmDetails.getVmName());
	}
}
