/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.operations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.ops.sdkimpl.VCenterHelper;
import com.hytrust.performance.benchmark.result.ResultAggregator;
import com.hytrust.performance.benchmark.util.SpringInitializer;


/**
 * @author rkonde
 *
 * Provides method for Snapshot VM using VI Java
 */
public class SnapshotVmOperation extends AbstractOperation {
	
	private Logger logger = Logger.getLogger(SnapshotVmOperation.class);
	
	/** 
     * Snapshot VM
     * If ServiceInstance is not present(Parallel Execution), login in to VCenter->Perform Operations and Logout
     * If ServiceInstance is present(Sequential Execution) , Perform Operation
     * Add OpResult for operations performed to list and adds list to ResultAggregator
     */
    @Override
    public void execute() {
        //System.out.println("Snapshot VM");
    	logger.info("Snapshot VM "+vmDetails.getVmName());
        // If SI is null, this is parallel exec. Do login and logoff in op.
        List<OpResult> results = new ArrayList<OpResult>();
        OpResult result =null;
        boolean parallel = false;

        if(this.si == null) {
        	result = createSession();
            if(result.getOperationStatus() == OperationStatus.EXCEPTION) {
              	return;
            }
            results.add(result);
            parallel = true;
        }
        
        results.addAll(vmOps.takeSnapshot(si, vmDetails.getVmName(), vmDetails.getVmName()+"_snapshot", 
                "snapshot of vm " + vmDetails.getVmName(), false, false));
        
        // Logoff and add vmdetails back
        if(parallel) {
        	VMContainerAccessor.add(vmDetails);
        	result = destroySession();
        	results.add(result);
        } else {
        	VMContainerAccessor.add(vmDetails.getVcUrl(), vmDetails);
        }
        
        ResultAggregator.getPerformanceResult().add(results);
        logger.info("Finished Snapshot VM "+vmDetails.getVmName());
    }

}
