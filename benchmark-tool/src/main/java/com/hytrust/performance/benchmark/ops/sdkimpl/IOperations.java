package com.hytrust.performance.benchmark.ops.sdkimpl;

import java.util.List;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.vmware.vim25.mo.ServiceInstance;

public interface IOperations {
	/*
	 * Host related operations
	 */
	List<OpResult> addHost(ServiceInstance si, String dcName);
	List<OpResult> deleteHost(ServiceInstance si, String dcName, String vcUrl);
	List<OpResult> rebootHost(ServiceInstance si);
	List<OpResult> disconnectHost(ServiceInstance si, String vcUrl);
	List<OpResult> reconnectHost(ServiceInstance si, String vcUrl);
	/*
	 * Network component related operations
	 */
	List<OpResult> addPortgroup(ServiceInstance si);
	List<OpResult> addVirtualSwitch(ServiceInstance si);
	List<OpResult> removeVirtualSwitch(ServiceInstance si);
	List<OpResult> removePortgroup(ServiceInstance si);
	List<OpResult> createCluster(ServiceInstance si, String dcName);
	/*
	 * Cluster related operations
	 */
	List<OpResult> addHostToCluster(ServiceInstance si, String dcName);
	List<OpResult> removeHostFromCluster(ServiceInstance si, String dcName);
}
