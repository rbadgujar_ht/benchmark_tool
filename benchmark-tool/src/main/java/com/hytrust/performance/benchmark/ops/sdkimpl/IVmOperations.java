/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.ops.sdkimpl;

import java.util.List;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.vmware.vim25.mo.ServiceInstance;

public interface IVmOperations {
	List<OpResult> createVm(ServiceInstance si, String vmName, String hostName, String dcName, String datastoreName);
	List<OpResult> deleteVm(ServiceInstance si, String vmName);
	List<OpResult> takeSnapshot(ServiceInstance si, String vmName, String snapShotName, String snapshotDesc, boolean quiesced, boolean mem);
	List<OpResult> powerOnVm(ServiceInstance si, String vmName);
	List<OpResult> powerOffVm(ServiceInstance si, String vmName);
	List<OpResult> resetVm(ServiceInstance si, String vmName);
	List<OpResult> rebootGuest(ServiceInstance si, String vmName);
	List<OpResult> migrateVM(ServiceInstance si, String vmName, String dcName, String hostName);
	List<OpResult> relocateVm(ServiceInstance si, String vmName, String dcName, String hostName);
	List<OpResult> cloneVm(ServiceInstance si, String vmName, String cloneName);
}
