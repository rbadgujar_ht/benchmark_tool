/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.ops.sdkimpl;

import com.vmware.vim25.*;
import org.springframework.stereotype.Service;

/**
 * @author rkonde
 * */

/**
 * This class creates NIC specification used by createVm operations
 * */
@Service
public class NicSpecificationCreatorImpl implements NicSpecificationCreator {

    @Override
    public VirtualDeviceConfigSpec createNicSpec(String netName, String nicName) {
        VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();
        nicSpec.setOperation(VirtualDeviceConfigSpecOperation.add);
        VirtualEthernetCard nic = new VirtualPCNet32();
        VirtualEthernetCardNetworkBackingInfo nicBacking = new VirtualEthernetCardNetworkBackingInfo();
        nicBacking.setDeviceName(netName);
        Description info = new Description();
        info.setLabel(nicName);
        info.setSummary(netName);
        nic.setDeviceInfo(info);
        // type: "generated", "manual", "assigned" by VC
        nic.setAddressType("generated");
        nic.setBacking(nicBacking);
        nic.setKey(0);
        nicSpec.setDevice(nic);
        return nicSpec;
    }
}
