/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.ops.sdkimpl;

import com.vmware.vim25.VirtualDeviceConfigSpec;

/**
 * Created by rahul on 11/10/14.
 */
public interface ScsiSpecificationCreator {

    VirtualDeviceConfigSpec createScsiSpec(int cKey);


}
