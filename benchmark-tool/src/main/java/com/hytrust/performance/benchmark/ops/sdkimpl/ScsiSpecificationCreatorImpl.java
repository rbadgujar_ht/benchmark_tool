/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.ops.sdkimpl;

import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualLsiLogicController;
import com.vmware.vim25.VirtualSCSISharing;
import org.springframework.stereotype.Service;

/**
 * @author rkonde
 * */

/**
 * This class creates SCSI specification used by createVm operations
 * */
@Service
public class ScsiSpecificationCreatorImpl implements ScsiSpecificationCreator {

    @Override
    public VirtualDeviceConfigSpec createScsiSpec(int cKey) {

        VirtualDeviceConfigSpec scsiSpec = new VirtualDeviceConfigSpec();
        scsiSpec.setOperation(VirtualDeviceConfigSpecOperation.add);
        VirtualLsiLogicController scsiCtrl = new VirtualLsiLogicController();
        if (cKey != 0) {
            scsiCtrl.setKey(cKey);
        }
        scsiCtrl.setBusNumber(0);
        scsiCtrl.setSharedBus(VirtualSCSISharing.noSharing);
        scsiSpec.setDevice(scsiCtrl);
        return scsiSpec;

    }
}
