/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.ops.sdkimpl;

import java.rmi.RemoteException;
import java.util.List;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.mo.Datacenter;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.ResourcePool;
import com.vmware.vim25.mo.ServiceInstance;

public interface VCenterHelper {
	Datacenter getDatacenter(ServiceInstance si, String dcName)throws RuntimeFault, RemoteException;
	HostSystem getHostSystemByName(ServiceInstance si, String hostName)throws RemoteException;
	ResourcePool getResourcePool(Datacenter dc, String hostName)throws RemoteException;
	ServiceInstance login(String url, String username, String password);
	List<HostDetails> getHostSystems(ServiceInstance si) throws RemoteException;
	String getDcNameForHost(ServiceInstance si, ManagedEntity me);
	String getHostForVM(ServiceInstance si, String vmName);
	ManagedEntity[] getClusters(ServiceInstance si) throws RemoteException;	
}
