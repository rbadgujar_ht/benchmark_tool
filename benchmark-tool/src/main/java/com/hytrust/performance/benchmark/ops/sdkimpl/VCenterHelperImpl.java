/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.ops.sdkimpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.HostSystemPowerState;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.mo.Datacenter;
import com.vmware.vim25.mo.Datastore;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.InventoryNavigator;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.ResourcePool;
import com.vmware.vim25.mo.SearchIndex;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.VirtualMachine;
/**
 * @author rkonde
 * */
/**
 * This class contains methods to login into vCenter and get different objects from vCenter.
 * */
@Service
public class VCenterHelperImpl implements VCenterHelper{

	private Logger logger = Logger.getLogger(VCenterHelperImpl.class);
	
	@Override
	public Datacenter getDatacenter(ServiceInstance si, String dcName) throws RuntimeFault, RemoteException {
		logger.debug("Get Datacenter "+dcName);
		SearchIndex searchIndex = si.getSearchIndex();
        return (Datacenter) searchIndex.findByInventoryPath(dcName);
	}

	@Override
	public HostSystem getHostSystemByName(ServiceInstance si, String hostName) throws RemoteException {
		logger.debug("Get HostSystem : "+hostName);
		ManagedEntity rootFolder = si.getRootFolder();
        ManagedEntity[] managedEntities = new InventoryNavigator(rootFolder).searchManagedEntities("HostSystem");

        for (ManagedEntity me : managedEntities) {
            if (me.getName().equals(hostName)) {
                return (HostSystem) me;
            }
        }
        return null;		
	}
	
	@Override
	public ManagedEntity[] getClusters(ServiceInstance si) throws RemoteException {
		logger.debug("fetching all clusters");
		ManagedEntity rootFolder = si.getRootFolder();
        ManagedEntity[] managedEntities = new InventoryNavigator(rootFolder).searchManagedEntities("ClusterComputeResource");
        return managedEntities;		
	}

	@Override
	public List<HostDetails> getHostSystems(ServiceInstance si) throws RemoteException {
		logger.debug("Get Host Details ");
		ManagedEntity rootFolder = si.getRootFolder();
        ManagedEntity[] managedEntities = new InventoryNavigator(rootFolder).searchManagedEntities("HostSystem");
        
        List<HostDetails> hosts = new ArrayList<HostDetails>();
         
        for (ManagedEntity me : managedEntities) {
			HostDetails hostDetails = new HostDetails();
			HostSystem hs = (HostSystem) me;
			hostDetails.setHostName(hs.getName());
			logger.info(" Host Name : "+hostDetails.getHostName());
			
			if(hs.getRuntime().getPowerState() != HostSystemPowerState.poweredOn) {
				logger.error("Skipping host "+hs.getName()+" Host is not poweredOn");
				continue;
			}
			
			Datastore[] datastores = hs.getDatastores();
								
			if(datastores.length == 0) {
				logger.error("Skipping host "+hs.getName()+" no Datastore info available");
				continue;
			}			
			
			hostDetails.setDataStoreName(hs.getDatastores()[0].getName());
			logger.info(" Datastore Name : "+hostDetails.getDataStoreName());
			String dcName = getDcNameForHost(si, me);
			
			if(dcName == null) {
				logger.error("Skipping host "+hs.getName()+" no DC info available");
				continue;
			}
			
			hostDetails.setDcName(dcName);
			logger.info(" Datacenter Name : "+hostDetails.getDcName());
			hosts.add(hostDetails);
		}
        
        return hosts;
	}
	
	@Override
	public ResourcePool getResourcePool(Datacenter dc, String hostName) throws RemoteException {
		logger.debug("Get ResourcePool DCName : "+dc.getName()+" HostName : "+hostName);
		ResourcePool rp = null;
		ManagedEntity me[] = new InventoryNavigator(dc).searchManagedEntities("ResourcePool");
		
		for (ManagedEntity managedEntity : me) {
			ResourcePool r = (ResourcePool)managedEntity;
			HostSystem[] hosts = r.getOwner().getHosts();
			for (HostSystem hostSystem : hosts) {
				if(hostName.equals(hostSystem.getName())) {
					rp = r;
					break;
				}
			}
		}
		//return (ResourcePool) new InventoryNavigator(dc).searchManagedEntities("ResourcePool")[0];
		return rp;
	}

	@Override
	public ServiceInstance login(String url, String username, String password) {
		logger.debug("Login to vCenter URL : "+url+" Username : "+username);
		try {
			return new ServiceInstance(new URL(url), username, password, true);
		} catch (RemoteException | MalformedURLException e) {
			//e.printStackTrace();
			logger.error("Login to vCenter URL : "+url+" Username : "+username+" failed with exception "+e.getLocalizedMessage());
			return null;
		}
	}
	
	@Override
	public String getDcNameForHost(ServiceInstance si, ManagedEntity me) {
		logger.debug("Get DC for host "+me.getName());
		String dcName=null;
		try {
			PropertyCollector pc = si.getPropertyCollector();

		    ManagedObjectReference mor = me.getMOR();
		    
		    SelectionSpec sspec1 = new SelectionSpec();
		    sspec1.setName("FolderParent");
		    
		    TraversalSpec tspec1 = new TraversalSpec();
		    tspec1.setName("FolderParent");
		    tspec1.setSkip(Boolean.FALSE);
		    tspec1.setType("Folder");
		    tspec1.setPath("parent");
		    tspec1.setSelectSet(new SelectionSpec[] { sspec1 });
		    
		    TraversalSpec tspec2 = new TraversalSpec();
		    tspec2.setSkip(Boolean.FALSE);
		    tspec2.setType("ComputeResource");
		    tspec2.setPath("parent");
		    tspec2.setSelectSet(new SelectionSpec[] { tspec1 });
		    
		    TraversalSpec tspec3 = new TraversalSpec();
		    tspec3.setSkip(Boolean.FALSE);
		    tspec3.setType("HostSystem");
		    tspec3.setPath("parent");
		    tspec3.setSelectSet(new SelectionSpec[] { tspec2 });
		    
		    ObjectSpec ospec1 = new ObjectSpec();
		    ospec1.setObj(mor);
		    ospec1.setSkip(Boolean.FALSE);
		    ospec1.setSelectSet(new SelectionSpec[] { tspec3 });
		    
		    PropertySpec pspec1 = new PropertySpec();
		    pspec1.setType("Datacenter");
		    pspec1.setPathSet(new String[] { "name" });
		    
		    PropertyFilterSpec pfSpec = new PropertyFilterSpec();
		    pfSpec.setObjectSet(new ObjectSpec[] { ospec1 });
		    pfSpec.setPropSet(new PropertySpec[] { pspec1 });
		    
		    ObjectContent[] oc = pc.retrieveProperties(new PropertyFilterSpec[] { pfSpec });
		    if (oc != null && oc.length == 1) {
		      DynamicProperty[] props = oc[0].getPropSet();
		      if (props != null && props.length == 1) {
		        dcName = (String) props[0].getVal();
		      }
		    }
		}catch(Exception e){
			logger.debug("Get DC for host "+me.getName()+" failed with exception "+e.getMessage());
			e.printStackTrace();
		}
		return dcName;
	}
	
	//TODO : Handle this call in startup and maintain host name in vmdetails 
	public String getHostForVM(ServiceInstance si, String vmName) {
		String hostName = null;
		
		try {
			ManagedEntity rootFolder = si.getRootFolder();
	        ManagedEntity[] managedEntities = new InventoryNavigator(rootFolder).searchManagedEntities("HostSystem");
	        
	        for (ManagedEntity managedEntity : managedEntities) {
				HostSystem host = (HostSystem) managedEntity;
				
				VirtualMachine[] vms = host.getVms();
				
				for (VirtualMachine vm : vms) {
					if(vm.getName().equals(vmName)) {
						hostName = host.getName();
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error in getHostForVm "+e.getMessage());
			e.printStackTrace();
		}
		
		return hostName;
	}	
}
