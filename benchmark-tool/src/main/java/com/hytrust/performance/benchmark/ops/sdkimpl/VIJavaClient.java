package com.hytrust.performance.benchmark.ops.sdkimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.beans.HostDetails;
import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.init.HostContainerAccessor;
import com.hytrust.performance.benchmark.properties.HostConfigs;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;
import com.vmware.vim25.ClusterConfigSpec;
import com.vmware.vim25.HostConnectSpec;
import com.vmware.vim25.HostNetworkPolicy;
import com.vmware.vim25.HostPortGroupConfig;
import com.vmware.vim25.HostPortGroupSpec;
import com.vmware.vim25.HostRuntimeInfo;
import com.vmware.vim25.HostSystemConnectionState;
import com.vmware.vim25.HostSystemPowerState;
import com.vmware.vim25.HostVirtualSwitchConfig;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.mo.ClusterComputeResource;
import com.vmware.vim25.mo.Datacenter;
import com.vmware.vim25.mo.Folder;
import com.vmware.vim25.mo.HostNetworkSystem;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.PropertyFilter;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.util.PropertyCollectorUtil;
/**
 * @author ttapkir
 *
 * This class implements Host Operations using VI Java API.
 * */
@Service
public class VIJavaClient implements IOperations{

    /*
    @Autowired
    private HostConfigs hostConfigs;*/
    
    @Autowired
    VCenterHelper vCenter;
    
    private Logger logger = Logger.getLogger(VIJavaClient.class);
	@Override
	public List<OpResult> addHost(ServiceInstance si,  String dcName) {
		List<OpResult> results = new ArrayList<OpResult>();		
		OpResult result= new OpResult();
		HostDetails hostDetails = null;
		boolean isAlreadyAdded = true;
		try {		
			if (HostContainerAccessor.getNonVcHostsSize() != 0) {
				hostDetails = HostContainerAccessor.getNonVcHost();
				isAlreadyAdded= false;
			} else {
				hostDetails = HostContainerAccessor.getVcHost();
				HostSystem host = vCenter.getHostSystemByName(si,
						hostDetails.getHostName());
				PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostDetails.getHostName());
				Task task = host.destroy_Task();
				PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si,result, logger, "DELETE_HOST", hostDetails.getHostName());

			}
			result.setNode(hostDetails.getHostName());
			result.setOperationType(OperationTypes.ADD_HOST);
			result.setStartTime(new Date());
			logger.debug("Add Host operation HostName : " + hostDetails.getHostName());
			Datacenter datacenter = vCenter.getDatacenter(si, dcName);
			Folder hostFolder = datacenter.getHostFolder();

			HostConnectSpec hostConnectSpec = new HostConnectSpec();
			hostConnectSpec.setForce(true);
			hostConnectSpec.setHostName(hostDetails.getHostName());
			hostConnectSpec.setUserName(hostDetails.getUsername());
			hostConnectSpec.setPassword(hostDetails.getPassword());
			hostConnectSpec.setSslThumbprint(hostDetails.getSslThumbPrint());

			Task task = hostFolder.addStandaloneHost_Task(hostConnectSpec, null, false);

			PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si,
					result, logger, "ADD_HOST", hostDetails.getHostName());
			if(isAlreadyAdded==false){
				HostSystem host = vCenter.getHostSystemByName(si, hostDetails.getHostName());
				if(host != null){
					HostSystemPowerState curState = host.getRuntime().getPowerState();
					
					if(curState == HostSystemPowerState.poweredOn) {
						Task t = host.shutdownHost_Task(true);
						t.waitForTask();
					}		
				
					Task t = host.destroy_Task();					
					PerformanceBenchmarkUtils.processTaskObjOfOperation(t, si, new OpResult(), logger, "DELETE_HOST", hostDetails.getHostName());
					
				}

			}
			logger.debug("Add Host operation HostName : " + hostDetails.getHostName() +" completed.");
    
		}catch(Exception e) {
			result.setEndTime(new Date());
        	result.setOperationStatus(OperationStatus.EXCEPTION);
        	logger.error("ADD_HOST"+hostDetails.getHostName()+" failed with exception "+e.getMessage());
		}
		finally{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
	@Override
	public List<OpResult> rebootHost(ServiceInstance si) {	
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = new OpResult();
		try {
			result.setNode(hostName);
			result.setOperationType(OperationTypes.REBOOT_HOST);
			result.setStartTime(new Date());
			
			HostSystem host = vCenter.getHostSystemByName(si, hostName); 
			
			if(host == null) {
				logger.error("REBOOT_HOST host not found HOSTNAME : "+hostName);
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				result.setMessage("REBOOT_HOST: host not found");
				results.add(result);
				return results;
			}
			logger.debug("Reboot Host operation on HostName : "+hostName);
			PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostName);
			HostSystemPowerState curState = host.getRuntime().getPowerState();
			
			if(curState == HostSystemPowerState.poweredOff) {
				logger.error("REBOOT_HOST: host is powered off : "+hostName);
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				result.setMessage("REBOOT_HOST: host is powered off");
				results.add(result);
				return results;
			}
			Task t = host.rebootHost(true);
			
			PerformanceBenchmarkUtils.processTaskObjOfOperation(t, si, result, logger, "REBOOT_HOST", hostName);
			logger.debug("Reboot Host operation on HostName : "+hostName+" completed.");
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			result.setMessage(e.getMessage());
			logger.error("REBOOT_HOST "+hostName+" failed with exception "+e.getMessage());
		}
		finally
		{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		return results;
	}
	@Override
	public List<OpResult> deleteHost(ServiceInstance si, String dcName, String vcUrl) {
		List<OpResult> results = new ArrayList<OpResult>();

		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = null;
		try {
			result = new OpResult();
			result.setNode(hostName);
			result.setOperationType(OperationTypes.DELETE_HOST);
			result.setStartTime(new Date());
			HostSystem host = vCenter.getHostSystemByName(si, hostName);
			if(host == null) {
				logger.error("DELETE_HOST host not found. HostName : "+hostName);
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				result.setMessage("DELETE_HOST : host not found.");
				results.add(result);
				return results;
			}
			logger.debug("Delete Host operation HostName : "+hostName);		
			PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostName);			
			Task task = host.destroy_Task();
			
			PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si, result, logger, "DELETE_HOST", hostName);
			if(result.getOperationStatus().equals(OperationStatus.SUCESS))
			{
				logger.debug("Removing VMs associated with deleted host : "+hostName);		
				PerformanceBenchmarkUtils.removeAllVmUnderHost(hostName, vcUrl);
				
				//Adding host again to keep infra in previous state
				Datacenter datacenter = vCenter.getDatacenter(si, dcName);
				Folder hostFolder = datacenter.getHostFolder();

				HostConnectSpec hostConnectSpec = new HostConnectSpec();
				hostConnectSpec.setForce(true);
				hostConnectSpec.setHostName(hostDetails.getHostName());
				hostConnectSpec.setUserName(hostDetails.getUsername());
				hostConnectSpec.setPassword(hostDetails.getPassword());
				hostConnectSpec.setSslThumbprint(hostDetails.getSslThumbPrint());

				Task t = hostFolder.addStandaloneHost_Task(hostConnectSpec, null, false);
				OpResult addHostResult = new OpResult();
				PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si,
						addHostResult, logger, "ADD_HOST", hostDetails.getHostName());
				if(addHostResult.getOperationStatus().equals(OperationStatus.SUCESS)){
					PerformanceBenchmarkUtils.addAllVmUnderHost(hostDetails.getHostName(), vcUrl);
				}
			}
			logger.debug("Delete Host operation HostName : "+hostName +" completed.");		
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			result.setMessage(e.getMessage());
			logger.error("DELETE_HOST "+hostName+" failed with exception "+e.getMessage());
		}
		finally
		{
			HostContainerAccessor.addNonVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
	@Override
	public List<OpResult> disconnectHost(ServiceInstance si, String vcUrl) {
		List<OpResult> results = new ArrayList<OpResult>();
		boolean alreadyDisconnected = false;
		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = new OpResult();
		try {
			result.setNode(hostName);
			result.setOperationType(OperationTypes.DISCONNECT_HOST);
			result.setStartTime(new Date());
			HostSystem requiredHost = vCenter.getHostSystemByName(si, hostName);
			
			if(requiredHost == null) {
				logger.error("DISCONNECT_HOST host not found ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				result.setMessage("DISCONNECT_HOST : host not found.");
				results.add(result);
				return results;
			}
			logger.debug("Disconnect Host operation on hostName : "+hostName);	
			PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostName);
			HostSystemConnectionState curState = requiredHost.getRuntime().getConnectionState();
			
			if(curState == HostSystemConnectionState.disconnected) {
				HostConnectSpec hcs = new HostConnectSpec();
				Task t = requiredHost.reconnectHost_Task(null);
				t.waitForTask();
				alreadyDisconnected = true;
			}				
			Task task = requiredHost.disconnectHost();			
			PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si, result, logger, "DISCONNECT_HOST", hostName);
			if(result.getOperationStatus().equals(OperationStatus.SUCESS))
			{
				logger.debug("Removing VMs associated with disconnected host : "+hostName);		
				PerformanceBenchmarkUtils.removeAllVmUnderHost(hostName, vcUrl);
				if(!alreadyDisconnected) {
					HostSystem host = vCenter.getHostSystemByName(si, hostName);
					HostConnectSpec hcs = new HostConnectSpec();
					Task t1 = host.reconnectHost_Task(null);
					t1.waitForTask();
				}
			}
			
			logger.debug("Disconnect Host operation on hostName : "+hostName+" completed.");		
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.SUCESS);
			result.setMessage(e.getMessage());
			logger.error("DISCONNECT_HOST operation failed with exception "+e.getLocalizedMessage());
		}
		finally
		{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}

	@Override
	public List<OpResult> reconnectHost(ServiceInstance si, String vcUrl) {	
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = new OpResult();
		try {
			result.setNode(hostName);
			result.setOperationType(OperationTypes.RECONNECT_HOST);
			result.setStartTime(new Date());					
			HostSystem requiredHost = vCenter.getHostSystemByName(si, hostName);			
			if(requiredHost == null) {
				logger.error("RECONNECT_HOST host not found ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				result.setMessage("RECONNECT_HOST : host not found.");
				results.add(result);
				return results;
			}
			logger.debug("Reconnect Host operation HostName : "+hostName);
					
			HostConnectSpec hcs = new HostConnectSpec();
			hcs.setHostName(hostName);
			hcs.setUserName(hostDetails.getUsername());
			hcs.setPassword(hostDetails.getPassword());
			hcs.setSslThumbprint(hostDetails.getSslThumbPrint());
			PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostName);
			Task task = requiredHost.reconnectHost_Task(hcs);
			
			PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si, result, logger, "RECONNECT_HOST", hostName);
			if(result.getOperationStatus().equals(OperationStatus.SUCESS)){
				PerformanceBenchmarkUtils.addAllVmUnderHost(hostDetails.getHostName(), vcUrl);
			}
			logger.debug("Reconnect Host operation HostName : "+hostName+" completed.");			
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.SUCESS);
			result.setMessage(e.getMessage());
			logger.error("RECONNECT_HOST operation failed with exception "+e.getLocalizedMessage());
		}
		finally
		{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
	@Override
	public List<OpResult> addVirtualSwitch(ServiceInstance si) {
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = null;
		String vSwitchName = hostName+"VSwitch"+PerformanceBenchmarkUtils.getNetComponentCount();
		try {
			result = new OpResult();
			result.setNode(hostName);
			result.setOperationType(OperationTypes.CREATE_VIRTUAL_SWITCH);
			result.setStartTime(new Date());
			
			HostSystem host = vCenter.getHostSystemByName(si, hostName);
			
			if(host != null)
			{
				logger.debug("AddVirtualSwitch operation hostName : "+hostName+" vSwitchName : "+vSwitchName);
				HostNetworkSystem hostNetworkSystem = host.getHostNetworkSystem();
				if(hostNetworkSystem != null)
				{
					hostNetworkSystem.addVirtualSwitch(vSwitchName, null);							
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.SUCESS);
					logger.debug("AddVirtualSwitch operation hostName : "+hostName+" vSwitchName : "+vSwitchName +" completed");
				}
				else
				{
					logger.error("ADD_VIRTUAL_SWITCH : Host network system not found. ");
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.FAIL);
					result.setMessage("Host network system not found.");
				}
			}else
			{
				logger.error("ADD_VIRTUAL_SWITCH : host not found to perform operation ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.FAIL);
				result.setMessage("Host not found.");
			}
			
		} catch (Exception e) {
			logger.error("ADD_VIRTUAL_SWITCH : exception occured : "+e.getMessage());
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			result.setMessage(e.getMessage());
		}finally{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
	
	@Override
	public List<OpResult> removeVirtualSwitch(ServiceInstance si) {
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = null;
		try {
			result = new OpResult();
			result.setNode(hostName);
			result.setOperationType(OperationTypes.REMOVE_VIRTUAL_SWITCH);
			result.setStartTime(new Date());
			
			HostSystem host = vCenter.getHostSystemByName(si, hostName);
			
			if(host != null)
			{
				logger.debug("REMOVE_VIRTUAL_SWITCH operation on hostName : "+hostName);
				HostNetworkSystem hostNetworkSystem = host.getHostNetworkSystem();
				String switchName= null;
				if(hostNetworkSystem != null)
				{
					HostVirtualSwitchConfig vSwitches[] =hostNetworkSystem.getNetworkConfig().getVswitch();
					if(vSwitches != null && vSwitches.length!=0)
					{
						switchName = vSwitches[0].getName();
					}else
					{
						String vSwitchName = hostName+"VSwitch"+PerformanceBenchmarkUtils.getNetComponentCount();
						hostNetworkSystem.addVirtualSwitch(vSwitchName, null);	
						switchName = vSwitchName;
					}
					
					hostNetworkSystem.removeVirtualSwitch(switchName);							
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.SUCESS);
					logger.debug("REMOVE_VIRTUAL_SWITCH operation hostName : "+hostName+" vSwitchName : "+switchName +" completed");

				}
				else
				{
					logger.error("REMOVE_VIRTUAL_SWITCH : Host network system not found. ");
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.FAIL);
					result.setMessage("Host network system not found.");
				}
			}else
			{
				logger.error("REMOVE_VIRTUAL_SWITCH : host not found to perform operation ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.FAIL);
				result.setMessage("Host not found.");
			}
			
		} catch (Exception e) {
			logger.error("REMOVE_VIRTUAL_SWITCH : exception occured : "+e.getMessage());
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			result.setMessage(e.getMessage());
		}finally{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
	
	
	@Override
	public List<OpResult> addPortgroup(ServiceInstance si) {
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = null;
		String portGroupName = hostName+"PortGroup"+PerformanceBenchmarkUtils.getNetComponentCount();
		try {
			result = new OpResult();
			result.setNode(hostDetails.getHostName());
			result.setOperationType(OperationTypes.ADD_PORTGROUP);
			result.setStartTime(new Date());
			
			HostSystem host = vCenter.getHostSystemByName(si, hostName);
			
			if(host != null)
			{
				HostNetworkSystem hostNetworkSystem = host.getHostNetworkSystem();
				if(hostNetworkSystem != null)
				{
					HostPortGroupSpec portGroupSpec = new HostPortGroupSpec();
					portGroupSpec.setName(portGroupName);
					portGroupSpec.setVlanId(0); 
					HostVirtualSwitchConfig vSwitches[] =hostNetworkSystem.getNetworkConfig().getVswitch();
					String switchName = null;
					if(vSwitches.length ==0)
					{
						String vSwitchName = hostName+"VSwitch"+PerformanceBenchmarkUtils.getNetComponentCount();
						hostNetworkSystem.addVirtualSwitch(vSwitchName, null);	
						switchName = vSwitchName;
					}
					else
					{
						switchName= vSwitches[0].getName();
					}
					logger.debug("ADD_PORTGROUP operation hostName : "+hostName+" vSwitchName : "+switchName);
					portGroupSpec.setVswitchName(switchName);
					portGroupSpec.setPolicy(new HostNetworkPolicy());
					hostNetworkSystem.addPortGroup(portGroupSpec);		
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.SUCESS);
					logger.debug("ADD_PORTGROUP operation hostName : "+hostName+" vSwitchName : "+switchName+" completed");				
				}
				else
				{
					logger.error("ADD_PORTGROUP : Host network system not found. ");
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.FAIL);
					result.setMessage("Host network system not found.");
				}
			}else
			{
				logger.error("ADD_PORTGROUP : host not found to perform operation ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.FAIL);
				result.setMessage("Host not found.");
			}
			
		} catch (Exception e) {
			logger.error("ADD_PORTGROUP : exception occured : "+e.getMessage());
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			result.setMessage(e.getMessage());
		}finally
		{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;	
	}
	
	@Override
	public List<OpResult> removePortgroup(ServiceInstance si) {
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= HostContainerAccessor.getVcHost();
		String hostName = hostDetails.getHostName();
		OpResult result = null;
		try {
			result = new OpResult();
			result.setNode(hostName);
			result.setOperationType(OperationTypes.REMOVE_PORTGROUP);
			result.setStartTime(new Date());
			
			HostSystem host = vCenter.getHostSystemByName(si, hostName);
			
			if(host != null)
			{
				logger.debug("REMOVE_PORTGROUP operation hostName : "+hostName);
				HostNetworkSystem hostNetworkSystem = host.getHostNetworkSystem();
				String pgName= null;
				if(hostNetworkSystem != null)
				{
					HostPortGroupConfig portGroups[] =hostNetworkSystem.getNetworkConfig().getPortgroup();
					if(portGroups != null && portGroups.length!=0)
					{						
						pgName = portGroups[0].getSpec().getName();
					}else
					{
						pgName = "PortGroup"+PerformanceBenchmarkUtils.getNetComponentCount();
						HostPortGroupSpec portGroupSpec = new HostPortGroupSpec();
						portGroupSpec.setName(pgName);
						portGroupSpec.setVlanId(0); 
						HostVirtualSwitchConfig vSwitches[] =hostNetworkSystem.getNetworkConfig().getVswitch();
						String switchName = null;
						if(vSwitches.length ==0)
						{
							String vSwitchName = hostName+"VSwitch"+PerformanceBenchmarkUtils.getNetComponentCount();
							hostNetworkSystem.addVirtualSwitch(vSwitchName, null);	
							switchName = vSwitchName;
						}
						else
						{
							switchName= vSwitches[0].getName();
						}
						portGroupSpec.setVswitchName(switchName);
						portGroupSpec.setPolicy(new HostNetworkPolicy());
						hostNetworkSystem.addPortGroup(portGroupSpec);	
					}
					
					hostNetworkSystem.removePortGroup(pgName);							
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.SUCESS);
					logger.debug("REMOVE_PORTGROUP operation hostName : "+hostName+" vSwitchName : "+pgName +" completed");

				}
				else
				{
					logger.error("REMOVE_PORTGROUP : Host network system not found. ");
					result.setEndTime(new Date());
					result.setOperationStatus(OperationStatus.FAIL);
					result.setMessage("Host network system not found.");
				}
			}else
			{
				logger.error("REMOVE_PORTGROUP : host not found to perform operation ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.FAIL);
				result.setMessage("Host not found.");
			}
			
		} catch (Exception e) {
			logger.error("REMOVE_PORTGROUP : exception occured : "+e.getMessage());
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			result.setMessage(e.getMessage());
		}finally{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
	
	@Override
	public List<OpResult> createCluster(ServiceInstance si,  String dcName) {
		List<OpResult> results = new ArrayList<OpResult>();		
		OpResult result= new OpResult();
		String clusterName= "cluster"+PerformanceBenchmarkUtils.getNetComponentCount();
		try {		
			
			result.setNode(clusterName);
			result.setOperationType(OperationTypes.CREATE_CLUSTER);
			result.setStartTime(new Date());
			logger.debug("Create cluster clusterName : " + clusterName);
			Datacenter datacenter = vCenter.getDatacenter(si, dcName);
			Folder hostFolder = datacenter.getHostFolder();
			ClusterComputeResource cluster = hostFolder.createCluster(clusterName, new ClusterConfigSpec());
			logger.debug("Create cluster clusterName : " + clusterName +" completed.");
    
		}catch(Exception e) {
			result.setEndTime(new Date());
        	result.setOperationStatus(OperationStatus.EXCEPTION);
        	logger.error("Create cluster clusterName : " + clusterName+" failed with exception "+e.getMessage());
		}
		results.add(result);
		return results;
	}
	
	@Override
	public List<OpResult> addHostToCluster(ServiceInstance si, String dcName) {	
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= null;
		OpResult result = new OpResult();
		try {
			ManagedEntity managedEntities[] = vCenter.getClusters(si);
			ClusterComputeResource cluster= null;
			if(managedEntities.length ==0)
			{
				String clusterName= "cluster"+PerformanceBenchmarkUtils.getNetComponentCount();
				Datacenter datacenter = vCenter.getDatacenter(si, dcName);
				Folder hostFolder = datacenter.getHostFolder();
				cluster = hostFolder.createCluster(clusterName, new ClusterConfigSpec());
			}else
			{
				Random r =new Random();
		        cluster = (ClusterComputeResource)managedEntities[r.nextInt(managedEntities.length)];
			}
			
			if (HostContainerAccessor.getNonVcHostsSize() != 0) {
				hostDetails = HostContainerAccessor.getNonVcHost();
			} else {
				hostDetails = HostContainerAccessor.getVcHost();
				HostSystem host = vCenter.getHostSystemByName(si,
						hostDetails.getHostName());
				PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostDetails.getHostName());
				Task task = host.destroy_Task();
				PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si,result, logger, "DELETE_HOST", hostDetails.getHostName());
				results.add(result);				
			}
			String hostName = hostDetails.getHostName();
			result = new OpResult();
			result.setNode(hostDetails.getHostName());
			result.setOperationType(OperationTypes.ADD_HOST_TO_CLUSTER);
			result.setStartTime(new Date());
			
			
			HostConnectSpec hostConnectSpec = new HostConnectSpec();
			hostConnectSpec.setForce(true);
			hostConnectSpec.setHostName(hostDetails.getHostName());
			hostConnectSpec.setUserName(hostDetails.getUsername());
			hostConnectSpec.setPassword(hostDetails.getPassword());
			hostConnectSpec.setSslThumbprint(hostDetails.getSslThumbPrint());
						
			if(cluster== null)
			{
				logger.error("cluster not found ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				result.setMessage("cluster not found.");
				results.add(result);
				return results;
			}
			logger.debug("ADD_HOST_TO_CLUSTER operation cluster name: " + cluster.getName() + " HostName: "+hostName);
			Task task = cluster.addHost_Task(hostConnectSpec, true, null);
			PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si, result, logger, "ADD_HOST_TO_CLUSTER", hostName);
			logger.debug("ADD_HOST_TO_CLUSTER operation HostName : "+hostName+" completed.");			
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.SUCESS);
			result.setMessage(e.getMessage());
			logger.error("ADD_HOST_TO_CLUSTER operation failed with exception "+e.getLocalizedMessage());
		}
		finally
		{
			HostContainerAccessor.addVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
	
	@Override
	public List<OpResult> removeHostFromCluster(ServiceInstance si, String dcName) {	
		List<OpResult> results = new ArrayList<OpResult>();
		HostDetails hostDetails= null;
		OpResult result = null;
		try {
			result= new OpResult();			
			result.setOperationType(OperationTypes.REMOVE_HOST_FROM_CLUSTER);
			result.setStartTime(new Date());
			ManagedEntity managedEntities[] = vCenter.getClusters(si);
			ClusterComputeResource cluster= null;
			logger.debug("REMOVE_HOST_FROM_CLUSTER finding cluster to perform operation on..");
			if(managedEntities.length ==0)
			{
				String clusterName= "cluster"+PerformanceBenchmarkUtils.getNetComponentCount();
				Datacenter datacenter = vCenter.getDatacenter(si, dcName);
				Folder hostFolder = datacenter.getHostFolder();
				cluster = hostFolder.createCluster(clusterName, new ClusterConfigSpec());
			}else
			{
				Random r =new Random();
		        cluster = (ClusterComputeResource)managedEntities[r.nextInt(managedEntities.length)];
			}			
			if(cluster== null)
			{
				logger.error("cluster not found ");
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				result.setMessage("cluster not found.");
				results.add(result);
				return results;
			}
			result.setNode(cluster.getName());
			logger.debug("REMOVE_HOST_FROM_CLUSTER selecting host to delete on cluster..");
			if(cluster.getHosts().length==0)
			{
				if (HostContainerAccessor.getNonVcHostsSize() != 0) {
					hostDetails = HostContainerAccessor.getNonVcHost();
				} else {
					//We will remove already added host and add it in cluster
					hostDetails = HostContainerAccessor.getVcHost();
					OpResult result1= new OpResult();
					result1.setNode(hostDetails.getHostName());
					result1.setOperationType(OperationTypes.DELETE_HOST);
					result1.setStartTime(new Date());
					HostSystem host = vCenter.getHostSystemByName(si,
							hostDetails.getHostName());
					OpResult result2= new OpResult();
					result2.setNode(hostDetails.getHostName());
					result2.setOperationType(OperationTypes.DELETE_HOST);
					result2.setStartTime(new Date());
					PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostDetails.getHostName());
					Task t1= host.enterMaintenanceMode(10, false);
					PerformanceBenchmarkUtils.processTaskObjOfOperation(t1, si,result2, logger, "ENTER_MAINTAINANCE_MODE", hostDetails.getHostName());
					
					Task task = host.destroy_Task();
					
					PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si,result1, logger, "DELETE_HOST", hostDetails.getHostName());
					results.add(result1);				
				}
				OpResult result2= new OpResult();
				result2.setNode(hostDetails.getHostName());
				result2.setOperationType(OperationTypes.ADD_HOST_TO_CLUSTER);
				result2.setStartTime(new Date());
				HostConnectSpec hostConnectSpec = new HostConnectSpec();
				hostConnectSpec.setForce(true);
				hostConnectSpec.setHostName(hostDetails.getHostName());
				hostConnectSpec.setUserName(hostDetails.getUsername());
				hostConnectSpec.setPassword(hostDetails.getPassword());
				hostConnectSpec.setSslThumbprint(hostDetails.getSslThumbPrint());
				Task task = cluster.addHost_Task(hostConnectSpec, true, null);
				PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si, result2, logger, "ADD_HOST_TO_CLUSTER", hostDetails.getHostName());
				results.add(result2);
			}
			Random r = new Random();
			HostSystem hosts[] = cluster.getHosts();
			HostSystem host = hosts[r.nextInt(hosts.length)];
			String hostName = host.getName();
			logger.debug("REMOVE_HOST_FROM_CLUSTER operation cluster name: " + cluster.getName() + " HostName: "+hostName);
			OpResult result3= new OpResult();
			result3.setNode(hostDetails.getHostName());			
			result3.setStartTime(new Date());
			PerformanceBenchmarkUtils.waitTillNoOpsOnHost(hostDetails.getHostName());
			Task t = host.enterMaintenanceMode(15, false);
			PerformanceBenchmarkUtils.processTaskObjOfOperation(t, si, result3, logger, "ENTER_MAINTAINANCE_MODE", hostDetails.getHostName());
			Task task = host.destroy_Task();
			
			PerformanceBenchmarkUtils.processTaskObjOfOperation(task, si, result, logger, "ADD_HOST_TO_CLUSTER", hostName);
			logger.debug("REMOVE_HOST_FROM_CLUSTER operation HostName : "+hostName+" completed.");			
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.SUCESS);
			result.setMessage(e.getMessage());
			logger.error("REMOVE_HOST_FROM_CLUSTER operation failed with exception "+e.getMessage());
		}
		finally
		{
			HostContainerAccessor.addNonVcHost(hostDetails);
		}
		results.add(result);
		return results;
	}
}
