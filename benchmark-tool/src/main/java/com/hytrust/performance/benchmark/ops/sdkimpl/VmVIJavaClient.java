/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.ops.sdkimpl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.properties.VmConfigs;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;
import com.vmware.vim25.InvalidCollectorVersion;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.ObjectUpdate;
import com.vmware.vim25.PropertyChange;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertyFilterUpdate;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.SelectionSpec;
import com.vmware.vim25.TaskInfo;
import com.vmware.vim25.TaskInfoState;
import com.vmware.vim25.TraversalSpec;
import com.vmware.vim25.UpdateSet;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualMachineCloneSpec;
import com.vmware.vim25.VirtualMachineConfigSpec;
import com.vmware.vim25.VirtualMachineFileInfo;
import com.vmware.vim25.VirtualMachineMovePriority;
import com.vmware.vim25.VirtualMachinePowerState;
import com.vmware.vim25.VirtualMachineRelocateDiskMoveOptions;
import com.vmware.vim25.VirtualMachineRelocateSpec;
import com.vmware.vim25.WaitOptions;
import com.vmware.vim25.mo.Datacenter;
import com.vmware.vim25.mo.Datastore;
import com.vmware.vim25.mo.Folder;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.InventoryNavigator;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.PropertyFilter;
import com.vmware.vim25.mo.ResourcePool;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.TaskManager;
import com.vmware.vim25.mo.VirtualMachine;
import com.vmware.vim25.mo.util.MorUtil;
import com.vmware.vim25.mo.util.PropertyCollectorUtil;
/**
 * @author rkonde
 *
 * This class implements VM Operations using VI Java API.
 * */
@Service
public class VmVIJavaClient implements IVmOperations {
	
	@Autowired
    private ScsiSpecificationCreator scsiSpecificationCreator;

    @Autowired
    private NicSpecificationCreator nicSpecificationCreator;
    
    @Autowired
    private VmConfigs vmConfigs;
    
    @Autowired
    VCenterHelper vCenter;
    
    private Logger logger = Logger.getLogger(VmVIJavaClient.class);
	    
	@Override
	public List<OpResult> createVm(ServiceInstance si, String vmName, String hostName, String dcName, String dataStoreName) {
		List<OpResult> results = new ArrayList<OpResult>();
		
		OpResult result = new OpResult();
		try {
			logger.debug("CREATE_VM VMName : "+vmName+" HostName : "+hostName+" DataCenterName : "+dcName+" DataStoreName : "+dataStoreName);
			
			//create vm config spec
	        VirtualMachineConfigSpec vmSpec = new VirtualMachineConfigSpec();
	        vmSpec.setName(vmName);
	        vmSpec.setAnnotation(vmConfigs.getVimAnnotation());
	        vmSpec.setMemoryMB(vmConfigs.getMemorySizeMB());
	        vmSpec.setNumCPUs(vmConfigs.getCpuCount());
	        vmSpec.setGuestId(vmConfigs.getGuestOsId());
	        logger.debug("Created VMConfig for VM "+vmName);
	        
	        Datacenter datacenter = vCenter.getDatacenter(si, dcName);

	        ResourcePool rp = vCenter.getResourcePool(datacenter, hostName);
	       
	        Folder vmFolder = datacenter.getVmFolder();

	        // create virtual devices
	        int cKey = 1000;
	        VirtualDeviceConfigSpec scsiSpec = scsiSpecificationCreator.createScsiSpec(cKey);
	        VirtualDeviceConfigSpec nicSpec = nicSpecificationCreator.createNicSpec(vmConfigs.getNetworkName(), vmConfigs.getNicName());

	        vmSpec.setDeviceChange(new VirtualDeviceConfigSpec[] { scsiSpec,
	                nicSpec
	        });
	        logger.debug("Created VMSpec for VM "+vmName);
	        
	        // create vm file info for the vmx file
	        VirtualMachineFileInfo vmfi = new VirtualMachineFileInfo();
	        vmfi.setVmPathName("[" + dataStoreName + "]");
	        vmSpec.setFiles(vmfi);
	        logger.debug("Created VMFile for VM "+vmName);

	        HostSystem host = vCenter.getHostSystemByName(si, hostName); 
	                
	        result.setNode(vmName);
	        result.setOperationType(OperationTypes.CREATE_VM);
	        result.setStartTime(new Date());
	        
	        Task task = vmFolder.createVM_Task(vmSpec, rp, host);
	        
	        String taskRef = task.getMOR().getVal();
	        	        
	        ObjectSpec objSpec = PropertyCollectorUtil.creatObjectSpec(task.getMOR(), Boolean.FALSE, null);
	        
	        PropertySpec pSpec = PropertyCollectorUtil.createPropertySpec(task.getMOR().getType(), false, new String[]{"info","info.state","info.result"});
	        
	        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
	        pfSpec.setObjectSet(new ObjectSpec[]{objSpec});
	        pfSpec.setPropSet(new PropertySpec[]{pSpec});
	                	        
	        PropertyCollector pc = si.getServerConnection().getServiceInstance().getPropertyCollector();
	        PropertyFilter pf = pc.createFilter(pfSpec, false);
	        
	        String version = "";
	        
	        String state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
	        
	        while (state.equals("unknown") || state.equals("running") || state.equals("queued")) {
	        	Thread.sleep(500);
	        	System.out.println("waiting for update in create vm ....."+vmName);
	        	state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
			}
	        
	        if(state.equals("success")) {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.SUCESS);
            	logger.info("CREATE_VM successfull "+vmName);
	        } else {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.ERROR);
            	logger.error("CREATE_VM failed "+vmName);
	        }
	        
	        pf.destroyPropertyFilter();	        
	       
		}catch(Exception e) {
			result.setEndTime(new Date());
        	result.setOperationStatus(OperationStatus.EXCEPTION);
        	logger.error("CREATE_VM "+vmName+" failed with exception "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}
	
	
	@Override
	public List<OpResult> deleteVm(ServiceInstance si, String vmName) {
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName,result);
			results.add(result);
			
			if(vm == null) {
				logger.error("DELETE_VM VM not found VMNAME : "+vmName);
				return results;
			}
			
			//TODO : Add result if required
			VirtualMachinePowerState curState = vm.getRuntime().getPowerState();
			
			if(curState == VirtualMachinePowerState.poweredOn) {
				Task t = vm.powerOffVM_Task();
				t.waitForTask();
			}
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.DELETE_VM);
			result.setStartTime(new Date());
			Task task = vm.destroy_Task();
			
			String taskRef = task.getMOR().getVal();
	        
	        ObjectSpec objSpec = PropertyCollectorUtil.creatObjectSpec(task.getMOR(), Boolean.FALSE, null);
	        
	        PropertySpec pSpec = PropertyCollectorUtil.createPropertySpec(task.getMOR().getType(), false, new String[]{"info","info.state","info.result"});
	        
	        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
	        pfSpec.setObjectSet(new ObjectSpec[]{objSpec});
	        pfSpec.setPropSet(new PropertySpec[]{pSpec});
	                	        
	        PropertyCollector pc = si.getServerConnection().getServiceInstance().getPropertyCollector();
	        PropertyFilter pf = pc.createFilter(pfSpec, false);
	        
	        String version = "";
	        
	        String state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
	        
	        while (state.equals("unknown") || state.equals("running") || state.equals("queued")) {
	        	Thread.sleep(500);
	        	System.out.println("waiting for update in delete vm ....."+vmName);
	        	state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
			}
	        
	        if(state.equals("success")) {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.SUCESS);
            	logger.info("DELETE_VM successfull "+vmName);
	        } else {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.ERROR);
            	logger.error("DELETE_VM failed "+vmName);
	        }
	        
	        pf.destroyPropertyFilter();
			
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.SUCESS);
			logger.error("DELETE_VM "+vmName+" failed with exception "+vmName);
		}
		results.add(result);
		return results;
	}

	@Override
	public List<OpResult> takeSnapshot(ServiceInstance si, String vmName, String snapShotName, String snapshotDesc, boolean quiesced, boolean mem) {
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			if(vm == null) {
				logger.error("SNAPSHOT_VM VM not found VMNAME : "+vmName);
				return results;
			}
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.SNAPSHOT_VM);
			result.setStartTime(new Date());
			Task t = vm.createSnapshot_Task(snapShotName, snapshotDesc, quiesced, mem);
			
			if(t.waitForTask() == Task.SUCCESS) {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.SUCESS);
				logger.info("SNAPSHOT_VM successfull "+vmName);
			} else {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				logger.error("SNAPSHOT_VM failed "+vmName);
			}
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.SUCESS);
			logger.error("SNAPSHOT_VM "+vmName+" failed with exception "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}

	@Override
	public List<OpResult> powerOnVm(ServiceInstance si, String vmName) {
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			if(vm == null) {
				logger.error("POWER_ON_VM VM not found VMNAME : "+vmName);
				return results;
			}
			
			//TODO : Add result if required
			VirtualMachinePowerState curState = vm.getRuntime().getPowerState();
			
			if(curState == VirtualMachinePowerState.poweredOn) {
				Task t = vm.powerOffVM_Task();
				t.waitForTask();
			}
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.POWER_ON_VM);
			result.setStartTime(new Date());
			Task t = vm.powerOnVM_Task(null);
			
			if(t.waitForTask() == Task.SUCCESS) {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.SUCESS);
				logger.info("POWER_ON_VM successfull "+vmName);
			} else {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				logger.error("POWER_ON_VM failed : "+vmName);
			}
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("POWER_ON_VM "+vmName+" failed with exception "+e.getLocalizedMessage());
		}	
		results.add(result);
		return results;
	}

	@Override
	public List<OpResult> powerOffVm(ServiceInstance si, String vmName) {
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {			
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			if(vm == null) {
				logger.error("POWER_OFF_VM VM not found VMNAME : "+vmName);
				return results;
			}
			
			VirtualMachinePowerState curState = vm.getRuntime().getPowerState();
			
			if(curState == VirtualMachinePowerState.poweredOff) {
				Task t = vm.powerOnVM_Task(null);
				t.waitForTask();
			}
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.POWER_OFF_VM);
			result.setStartTime(new Date());
			Task t = vm.powerOffVM_Task();
			
			if(t.waitForTask() == Task.SUCCESS) {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.SUCESS);
				logger.info("POWER_OFF_VM successfull "+vmName);
			} else {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				logger.error("POWER_OFF_VM failed "+vmName);
			}
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("POWER_OFF_VM "+vmName+" failed with exception "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}

	@Override
	public List<OpResult> resetVm(ServiceInstance si, String vmName) {
		
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {			
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			if(vm == null) {
				logger.error("RESET_VM VM not found VMNAME : "+vmName);
				return results;
			}
			
			
			VirtualMachinePowerState curState = vm.getRuntime().getPowerState();
			
			if(curState == VirtualMachinePowerState.poweredOff) {
				Task t = vm.powerOnVM_Task(null);
				t.waitForTask();
			}
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.REBOOT_VM);
			result.setStartTime(new Date());
			Task t = vm.resetVM_Task();
			
			if(t.waitForTask() == Task.SUCCESS) {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.SUCESS);
				logger.info("RESET_VM successfull "+vmName);
			} else {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				logger.error("RESET_VM failed "+vmName);
			}
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("RESET_VM "+vmName+" failed with exception "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}
	
	@Override
	public List<OpResult> rebootGuest(ServiceInstance si, String vmName) {
		
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {
			
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			if(vm == null) {
				logger.error("REBOOT_VM VM not found VMNAME : "+vmName);
				return results;
			}
									
			VirtualMachinePowerState curState = vm.getRuntime().getPowerState();
			
			if(curState == VirtualMachinePowerState.poweredOff) {
				Task t = vm.powerOnVM_Task(null);
				t.waitForTask();
			}
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.REBOOT_VM);
			result.setStartTime(new Date());
			vm.rebootGuest();
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.SUCESS);
			logger.info("REBOOT_VM Done : "+vmName);
			
		} catch(Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("REBOOT_VM "+vmName+" failed with exception  "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}
		
	public VirtualMachine getVm(ServiceInstance si, String vmName,OpResult opResult) {
		VirtualMachine vm = null;
		logger.info("GET_VM VMNAME : "+vmName);
		opResult.setNode(vmName);
		opResult.setOperationType(OperationTypes.GET_VM);
		opResult.setStartTime(new Date());
		Folder rootFolder = si.getRootFolder();		
		try {
			vm = (VirtualMachine) new InventoryNavigator(rootFolder).searchManagedEntity("VirtualMachine", vmName);					
			opResult.setEndTime(new Date());
			opResult.setOperationStatus(OperationStatus.SUCESS);			
			logger.info("GET_VM VMNAME successfull: "+vmName);
		} catch(Exception e) {
			opResult.setEndTime(new Date());
			opResult.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("GET_VM "+vmName+" failed with exception "+e.getLocalizedMessage());
		}
		return vm;
	}

	public List<OpResult> migrateVM(ServiceInstance si, String vmName, String dcName, String hostName) {		
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			Datacenter datacenter = vCenter.getDatacenter(si, dcName);

	        ResourcePool rp = vCenter.getResourcePool(datacenter, hostName);
			
			HostSystem host = vCenter.getHostSystemByName(si, hostName); 
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.MIGRATE_VM);
			result.setStartTime(new Date());
			Task t = vm.migrateVM_Task(rp, host, VirtualMachineMovePriority.defaultPriority, null);
			if(t.waitForTask() == Task.SUCCESS) {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.SUCESS);
				logger.info("MIGRATE_VM successfull "+vmName);
			} else {
				result.setEndTime(new Date());
				result.setOperationStatus(OperationStatus.ERROR);
				logger.error("MIGRATE_VM failed "+vmName);
			}
		} catch (Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("MIGRATE_VM "+vmName+" failed with exception  "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}
	
	@Override
	public List<OpResult> relocateVm(ServiceInstance si, String vmName, String dcName, String hostName) {
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			Datacenter datacenter = vCenter.getDatacenter(si, dcName);

	        ResourcePool rp = vCenter.getResourcePool(datacenter, hostName);
			
			HostSystem host = vCenter.getHostSystemByName(si, hostName); 
			
			Datastore ds = host.getDatastores()[0];
			
			VirtualMachineRelocateSpec vmRel = new VirtualMachineRelocateSpec();
			vmRel.setDatastore(ds.getMOR());
			vmRel.setHost(host.getMOR());
			vmRel.setPool(rp.getMOR());
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.MIGRATE_VM);
			result.setStartTime(new Date());
			Task task = vm.relocateVM_Task(vmRel, VirtualMachineMovePriority.defaultPriority);
			
			String taskRef = task.getMOR().getVal();
	        
	        ObjectSpec objSpec = PropertyCollectorUtil.creatObjectSpec(task.getMOR(), Boolean.FALSE, null);
	        
	        PropertySpec pSpec = PropertyCollectorUtil.createPropertySpec(task.getMOR().getType(), false, new String[]{"info","info.state","info.result"});
	        
	        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
	        pfSpec.setObjectSet(new ObjectSpec[]{objSpec});
	        pfSpec.setPropSet(new PropertySpec[]{pSpec});
	                	        
	        PropertyCollector pc = si.getServerConnection().getServiceInstance().getPropertyCollector();
	        PropertyFilter pf = pc.createFilter(pfSpec, false);
	        
	        String version = "";
	        
	        String state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
	        
	        while (state.equals("unknown") || state.equals("running") || state.equals("queued")) {
	        	Thread.sleep(500);
	        	System.out.println("waiting for update in clone vm ....."+vmName);
	        	state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
			}
	        
	        if(state.equals("success")) {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.SUCESS);
            	logger.info("MIGRATE_VM successfull "+vmName);
	        } else {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.ERROR);
            	logger.error("MIGRATE_VM failed "+vmName);
	        }
	        
	        pf.destroyPropertyFilter();
			
			
		} catch (Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("MIGRATE_VM "+vmName+" failed with exception  "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}
	
	@Override
	public List<OpResult> cloneVm(ServiceInstance si, String vmName, String cloneName) {
		List<OpResult> results = new ArrayList<OpResult>();
		OpResult result = null;
		try {
			result = new OpResult();
			VirtualMachine vm = getVm(si, vmName, result);
			results.add(result);
			
			VirtualMachineCloneSpec cloneSpec = new VirtualMachineCloneSpec();
			VirtualMachineRelocateSpec relSpec = new VirtualMachineRelocateSpec();
			relSpec.setDiskMoveType(VirtualMachineRelocateDiskMoveOptions.moveAllDiskBackingsAndDisallowSharing.toString());
			cloneSpec.setLocation(relSpec);
			cloneSpec.setPowerOn(false);
			cloneSpec.setTemplate(false);
			
			result = new OpResult();
			result.setNode(vmName);
			result.setOperationType(OperationTypes.CLONE_VM);
			result.setStartTime(new Date());
			Task task = vm.cloneVM_Task((Folder) vm.getParent(), cloneName, cloneSpec);
			
			String taskRef = task.getMOR().getVal();
	        
	        ObjectSpec objSpec = PropertyCollectorUtil.creatObjectSpec(task.getMOR(), Boolean.FALSE, null);
	        
	        PropertySpec pSpec = PropertyCollectorUtil.createPropertySpec(task.getMOR().getType(), false, new String[]{"info","info.state","info.result"});
	        
	        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
	        pfSpec.setObjectSet(new ObjectSpec[]{objSpec});
	        pfSpec.setPropSet(new PropertySpec[]{pSpec});
	                	        
	        PropertyCollector pc = si.getServerConnection().getServiceInstance().getPropertyCollector();
	        PropertyFilter pf = pc.createFilter(pfSpec, false);
	        
	        String version = "";
	        
	        String state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
	        
	        while (state.equals("unknown") || state.equals("running") || state.equals("queued")) {
	        	Thread.sleep(500);
	        	System.out.println("waiting for update in clone vm ....."+vmName);
	        	state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
			}
	        
	        if(state.equals("success")) {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.SUCESS);
            	logger.info("CLONE_VM successfull "+vmName);
	        } else {
	        	result.setEndTime(new Date());
            	result.setOperationStatus(OperationStatus.ERROR);
            	logger.error("CLONE_VM failed "+vmName);
	        }
	        
	        pf.destroyPropertyFilter();
			
		} catch (Exception e) {
			result.setEndTime(new Date());
			result.setOperationStatus(OperationStatus.EXCEPTION);
			logger.error("CLONE_VM "+vmName+" failed with exception  "+e.getLocalizedMessage());
		}
		results.add(result);
		return results;
	}

	public List<OpResult> addHost(ServiceInstance si, String vmName,
			String hostName, String dcName, String datastoreName) {
		// TODO Auto-generated method stub
		return null;
	}
}
