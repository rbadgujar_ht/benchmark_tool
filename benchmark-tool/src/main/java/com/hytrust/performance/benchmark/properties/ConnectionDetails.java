/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.properties;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hytrust.performance.benchmark.beans.VCenterDetails;

/**
 * @author rkonde
 * */

/**
 * Reads connection details to vCenter from configuration file
 * */

@Service
public class ConnectionDetails implements InitializingBean{
	
	private int noOfVcenters;
	
	private List<VCenterDetails> vCenters=new ArrayList<VCenterDetails>();
	
	private Logger logger = Logger.getLogger(ConnectionDetails.class);
	
	
	public void afterPropertiesSet() throws Exception {
		logger.info("Started reading connection details");
		File fXmlFile = new ClassPathResource("config/connectionDetails.xml").getFile();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		
		NodeList node = doc.getElementsByTagName("no-of-vcenter");
		
		setNoOfVcenters(Integer.parseInt(node.item(0).getTextContent()));
		
		NodeList nList = doc.getElementsByTagName("vcenter");
	 
		for (int i = 0; i < nList.getLength(); i++) {
	 
			Node nNode = nList.item(i);
	
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
				Element eElement = (Element) nNode;
	 
				VCenterDetails vCenter = new VCenterDetails();
				
				NodeList urlList = eElement.getElementsByTagName("url");
				NodeList unameList = eElement.getElementsByTagName("username");
				NodeList passList = eElement.getElementsByTagName("password");
				
				if(urlList == null || unameList == null || passList == null) {
					logger.error(" Invalid input in connection-details.xml.....");
					System.exit(0);
				}
				
				Node url = urlList.item(0);
				Node uname = unameList.item(0);
				Node pass = passList.item(0);
				
				if(url == null || uname == null || pass == null) {
					logger.error(" Invalid input in connection-details.xml.....");
					System.exit(0);
				}
				
				vCenter.setUrl(url.getTextContent());
				vCenter.setUsername(uname.getTextContent());
				vCenter.setPassword(pass.getTextContent());
				
				getvCenters().add(vCenter);
			}
		}
		logger.info("Finished reading connection details");
	}

	public int getNoOfVcenters() {
		return noOfVcenters;
	}

	public void setNoOfVcenters(int noOfVcenters) {
		this.noOfVcenters = noOfVcenters;
	}

	public List<VCenterDetails> getvCenters() {
		return vCenters;
	}
	
	public void setvCenters(List<VCenterDetails> vCenters) {
		this.vCenters = vCenters;
	}	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "No of VCenters : "+noOfVcenters+" "+vCenters;
	}
}
