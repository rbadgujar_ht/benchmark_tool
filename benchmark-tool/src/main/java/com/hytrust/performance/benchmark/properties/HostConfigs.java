package com.hytrust.performance.benchmark.properties;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.hytrust.performance.benchmark.beans.HostDetails;

/**
 * @author ttapkir
 * */
/**
 * Read configuration from properties file to create host
 * */
@Service
public class HostConfigs implements InitializingBean{	
	private List<HostDetails> hosts=new ArrayList<HostDetails>();
	
	private Logger logger = Logger.getLogger(HostConfigs.class);
	
	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("Started reading host details");
		File fXmlFile = new ClassPathResource("config/hostConfig.xml").getFile();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		
		NodeList nList = doc.getElementsByTagName("host");
	 
		for (int i = 0; i < nList.getLength(); i++) {
	 
			Node nNode = nList.item(i);
	
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
				Element eElement = (Element) nNode;
	 
				HostDetails hostDetails = new HostDetails();
				
				NodeList hostNameList = eElement.getElementsByTagName("name");
				NodeList unameList = eElement.getElementsByTagName("username");
				NodeList passList = eElement.getElementsByTagName("password");
				NodeList sslThumbprintList = eElement.getElementsByTagName("sslThumbPrint");
				
				if(hostNameList == null || unameList == null || passList == null) {
					logger.error(" Invalid input in hostConfig.xml.....");
					System.exit(0);
				}
				
				Node hostName = hostNameList.item(0);
				Node uname = unameList.item(0);
				Node pass = passList.item(0);
				Node sslThumbPrint = sslThumbprintList.item(0);
				
				if(hostName == null || uname == null || pass == null) {
					logger.error(" Invalid input in hostConfig.xml.....");
					System.exit(0);
				}
				
				hostDetails.setHostName(hostName.getTextContent());
				hostDetails.setUsername(uname.getTextContent());
				hostDetails.setPassword(pass.getTextContent());
				hostDetails.setSslThumbPrint(sslThumbPrint.getTextContent());
				getHosts().add(hostDetails);
			}
		}
		logger.info("Finished reading host details");
		
	}

	public List<HostDetails> getHosts() {
		return hosts;
	}

	public void setHosts(List<HostDetails> hosts) {
		this.hosts = hosts;
	}


	
}
