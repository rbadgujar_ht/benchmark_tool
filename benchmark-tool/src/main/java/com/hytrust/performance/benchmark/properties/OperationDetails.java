/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.properties;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.enums.OperationTypes;

/**
 * @author rkonde
 * */

/**
 * Read Operation details provided in configuration file
 * */
@Service
public class OperationDetails implements InitializingBean{
	
	@Autowired
	@Qualifier("opProperties")
	private Properties opProperties;
	
	@Value("#{opProperties['total.operations.number']}")
	private int totalNoOfOperation;
	
	@Value("#{opProperties['execution.type']}")
    private String executionType;
	
	private Map<OperationTypes, Integer> mapOfConfiguredOpsNoOfOps = new HashMap<OperationTypes, Integer>();
	
	private Logger logger = Logger.getLogger(OperationDetails.class);
	
	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("Started calculating no of operations from percentage");
		Integer totalOps = getTotalNoOfOperation();
		int count = 0;
        for(OperationTypes operationType : OperationTypes.values()) {
            Integer opsPercentage = getValue(operationType);
            Double opsPerType = Math.ceil((opsPercentage/100.0) * totalOps);
            getMapOfConfiguredOpsNoOfOps().put(operationType, opsPerType.intValue());
            count += opsPerType.intValue();
        }
        /* If number of operations calculated is float, then it is rounded off to nearest max integer.
         * Due to this total number of operations executed may be greater than that specified in 
         * properties file.*/ 
        setTotalNoOfOperation(count);
        logger.info("Finished calculating no of operations from percentage");
	}
	
	public int getTotalNoOfOperation() {
		return totalNoOfOperation;
	}

	public void setTotalNoOfOperation(int totalNoOfOperation) {
		this.totalNoOfOperation = totalNoOfOperation;
	}

	public Properties getOpProperties() {
		return opProperties;
	}

	public void setOpProperties(Properties opProperties) {
		this.opProperties = opProperties;
	}		
	
	public int getValue(OperationTypes opType) {
		int value = Integer.parseInt(opProperties.getProperty(opType.toString(), "0"));
		return value;
	}

    public String getExecutionType() {
        return executionType;
    }

    public void setExecutionType(String executionType) {
        this.executionType = executionType;
    }

	public Map<OperationTypes, Integer> getMapOfConfiguredOpsNoOfOps() {
		return mapOfConfiguredOpsNoOfOps;
	}

	public void setMapOfConfiguredOpsNoOfOps(
			Map<OperationTypes, Integer> mapOfConfiguredOpsNoOfOps) {
		this.mapOfConfiguredOpsNoOfOps = mapOfConfiguredOpsNoOfOps;
	}    
}
