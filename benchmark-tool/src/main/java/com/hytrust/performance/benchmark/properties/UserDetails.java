/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author rkonde
 * */

/**
 * Reads user details provided in configuration file.
 * */
@Service
public class UserDetails {
	
	@Value("#{userProperties['number.users.sessions']}")
	private int noOfUserSessions;

	public int getNoOfUserSessions() {
		return noOfUserSessions;
	}

	public void setNoOfUserSessions(int noOfUserSessions) {
		this.noOfUserSessions = noOfUserSessions;
	}	
}
