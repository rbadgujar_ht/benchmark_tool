/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author rkonde
 * */

/**
 * Read configuration from properties file to create vms
 * */

@Service
public class VmConfigs {

	@Value("#{vmConfig['vm.memorySizeMB']}")
    private long memorySizeMB;

    @Value("#{vmConfig['vm.cpuCount']}")
    private int cpuCount;

    @Value("#{vmConfig['vm.guestOsId']}")
    private String guestOsId;

    @Value("#{vmConfig['vm.networkName']}")
    private String networkName;

    @Value("#{vmConfig['vm.nicName']}")
    private String nicName;

    @Value("#{vmConfig['vm.vimAnnotation']}")
    private String vimAnnotation;

	public long getMemorySizeMB() {
		return memorySizeMB;
	}

	public void setMemorySizeMB(long memorySizeMB) {
		this.memorySizeMB = memorySizeMB;
	}

	public int getCpuCount() {
		return cpuCount;
	}

	public void setCpuCount(int cpuCount) {
		this.cpuCount = cpuCount;
	}

	public String getGuestOsId() {
		return guestOsId;
	}

	public void setGuestOsId(String guestOsId) {
		this.guestOsId = guestOsId;
	}

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public String getNicName() {
		return nicName;
	}

	public void setNicName(String nicName) {
		this.nicName = nicName;
	}

	public String getVimAnnotation() {
		return vimAnnotation;
	}

	public void setVimAnnotation(String vimAnnotation) {
		this.vimAnnotation = vimAnnotation;
	}
}
