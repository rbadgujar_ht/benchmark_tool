/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.PerfResult;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;

public class ResultAggregator {

    /**
     * Holds the results per operation. Each operation returns a list of OpResults as an operation may involve 
     * multiple steps in it. 
     */
    public static BlockingQueue<List<OpResult>> performanceResult = 
            new LinkedBlockingQueue<List<OpResult>>();
    
    private static List<List<OpResult>> operationResults = null;
    
    public static BlockingQueue<List<OpResult>> getPerformanceResult() {
		return performanceResult;
	}
    
    /**
     * Map holding detailed results per operation type.
     */
    private static Map<OperationTypes, PerfResult> aggregateResult = new HashMap<OperationTypes, PerfResult>();
    
    /**
     * Aggregates the results emitted by all the operations and returns map of operation type and detailed 
     * performance numbers associated with it. 
     * @return
     */
    public static Map<OperationTypes, PerfResult> aggregate() {
        PerfResult perfResult = null;
        setOperationResults(new ArrayList<List<OpResult>>(performanceResult.size()));
        
        while(!performanceResult.isEmpty()) {
            try {
                List<OpResult> opResults = performanceResult.take();
                getOperationResults().add(opResults);
                for (OpResult opResult : opResults) {
                	
                	if(opResult.getOperationStatus() == OperationStatus.SUCESS) {
                		OperationTypes operationType = opResult.getOperationType();
                        Long startTime = opResult.getStartTime().getTime();
                        Long opTime = opResult.getEndTime().getTime() - startTime;
                        if(aggregateResult.get(operationType) == null) {
                            perfResult = new PerfResult();
                            perfResult.setOperationType(operationType);
                            perfResult.setAverageTime(new Float(opTime));
                            perfResult.setTotalTime(opTime);
                            perfResult.setTotalNumberOfOperations(1);
                            aggregateResult.put(operationType, perfResult);
                        } else {
                            perfResult = aggregateResult.get(operationType);
                            perfResult.setTotalNumberOfOperations(perfResult.getTotalNumberOfOperations() + 1);
                            Long temp =  perfResult.getTotalTime() + opTime;
                            perfResult.setTotalTime(temp);
                            perfResult.setAverageTime(new Float(temp)/perfResult.getTotalNumberOfOperations());
                            //aggregateResult.put(operationType, perfResult);
                        }
                	}            	
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return aggregateResult;
    }
    
    public static void print() {
        for (OperationTypes opType : aggregateResult.keySet()) {
            System.out.println("Average time for " + opType.name() + " is " + aggregateResult.get(opType) + " milliseconds");
        }
    }

    public static List<List<OpResult>> getOperationResults() {
        return operationResults;
    }

    public static void setOperationResults(List<List<OpResult>> operationResults) {
        ResultAggregator.operationResults = operationResults;
    }
}
