/*
   * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.hytrust.performance.benchmark.beans.OpResult;
import com.hytrust.performance.benchmark.beans.PerfResult;
import com.hytrust.performance.benchmark.enums.OperationStatus;
import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.init.HostContainerAccessor;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.vmware.vim25.InvalidCollectorVersion;
import com.vmware.vim25.InvalidProperty;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.ObjectUpdate;
import com.vmware.vim25.PropertyChange;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertyFilterUpdate;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.UpdateSet;
import com.vmware.vim25.WaitOptions;
import com.vmware.vim25.mo.PropertyCollector;
import com.vmware.vim25.mo.PropertyFilter;
import com.vmware.vim25.mo.ServiceInstance;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.util.PropertyCollectorUtil;

public class PerformanceBenchmarkUtils {
    
    public static int count=0;
    public static int networkComponentCount=0;
    
    public synchronized static String getVmName() {
        count++;
        return "vm_perf_htcc_"+count;
    }
    
    public synchronized static int getNetComponentCount() {
    	networkComponentCount++;
        return networkComponentCount;
    }
    /**
     * Prints the performance result in a file. 
     * @param outputFile
     * @param results
     */
    public static void writePerfResults(File outputFile, Map<OperationTypes, PerfResult> results) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(outputFile);
            Integer totalNumberOfOperations = 0;
            //Print Total time taken per operation type 
            fileWriter.write("***** Average Time Per Operation Type *****\n");
            fileWriter.write("-------------------------------------------\n");
            for (OperationTypes opType : results.keySet()) {
                PerfResult perfResult = results.get(opType);
                totalNumberOfOperations+=perfResult.getTotalNumberOfOperations();
                fileWriter.write("Total time for " + opType.name() + " is " 
                        + perfResult.getTotalTime() + " milliseconds.\n");
                fileWriter.write("Average time for " + opType.name() + " is " 
                        + perfResult.getAverageTime() + " milliseconds.\n");
                fileWriter.write("Number of operations performed for " + opType.name() 
                        + " is " + perfResult.getTotalNumberOfOperations() + "\n");
                fileWriter.write("\n\n");
            }
            fileWriter.write("Total number of operations performed is " + totalNumberOfOperations);
            fileWriter.flush();
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * @return
     */
    public static Integer getMaxRunnableThreads() {
        /* TODO : Check what is optimal number of Threads that can be started. Right now this is hard coded,
         * check if hyperthreading enabled and change the login if needed.
        */
        return Runtime.getRuntime().availableProcessors() * 2;
    }
    
    /**
     * Read serialized result
     * @param serializedFile
     * @return
     */
    public static Map<OperationTypes, PerfResult> readSerializedPerfResults(File serializedFile) {
        FileInputStream fin = null;
        ObjectInputStream oin = null;
        Map<OperationTypes, PerfResult> results = null;
        try {
            fin = new FileInputStream(serializedFile);
            oin = new ObjectInputStream(fin);
            results = (Map<OperationTypes, PerfResult>) oin.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }finally {
            try {
                if(fin != null) {
                    fin.close();
                }
                if(oin != null) {
                    oin.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return results;        
    }
    
    /**
     * Serialize results to a file
     * @param results
     * @param serializedFile
     */
    public static void writeSerializedPerfResults(Map<OperationTypes, PerfResult> results, File serializedFile) {
        FileOutputStream fout = null;
        ObjectOutputStream oo = null;
        try {
            fout = new FileOutputStream(serializedFile);
            oo = new ObjectOutputStream(fout);
            oo.writeObject(results);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fout != null) {
                    fout.close();
                }
                if(oo != null) {
                    oo.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }        
    }
    
    public static void writeDetailedOpResults(List<List<OpResult>> operationResults, File resultsFile) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(resultsFile);
            Integer totalNumberOfOperations = 0;
            //Print Total time taken per operation type 
            fileWriter.write("***** Per Operation Detailed Results *****\n");
            fileWriter.write("------------------------------------------\n\n");
            for (List<OpResult> opResults : operationResults) {
                fileWriter.write("The vmname is : " + opResults.get(0).getNode() + "\n");
                for (OpResult opResult : opResults) {
                    totalNumberOfOperations++;
                    fileWriter.write("The operation type is : " + opResult.getOperationType().name() + "\n");
                    fileWriter.write("The operation status is : " + opResult.getOperationStatus().name() + "\n");
                    fileWriter.write("The operation start time is : " + opResult.getStartTime() + "\n");
                    fileWriter.write("The operation end time is : " + opResult.getEndTime() + "\n");
                    fileWriter.write("Time taken for operation is : " + 
                            (opResult.getEndTime().getTime()-opResult.getStartTime().getTime()) + "\n");
                    fileWriter.write("The operation message is : " + opResult.getMessage() + "\n");
                    fileWriter.write("\n");
                }
                fileWriter.write("------------------------------------------------------\n\n");
            }
            fileWriter.write("Total number of operations performed is " + totalNumberOfOperations);
            fileWriter.flush();
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void deleteResultsFile(File resultsFile) {
    	if(resultsFile.exists()) {
    		resultsFile.delete();
    	}
    }
    public static String waitForUpdates(ServiceInstance si, PropertyCollector pc, String version, String taskRef) throws InvalidCollectorVersion, RuntimeFault, RemoteException {		
		String state = "unknown";

		WaitOptions wait = new WaitOptions();
		wait.setMaxWaitSeconds(2 * 1000);

		UpdateSet upSet = pc.waitForUpdatesEx(version, wait);

		version = upSet.getVersion();

		PropertyFilterUpdate[] pfUp = upSet.getFilterSet();
        
        for (PropertyFilterUpdate pfUpdate : pfUp) {
			
        	ObjectUpdate[] objUp = pfUpdate.getObjectSet();
        	
        	for (ObjectUpdate objectUpdate : objUp) {
				if(objectUpdate.getObj().getVal().equals(taskRef)) {
					
					PropertyChange[] prop = objectUpdate.getChangeSet();
					
					for (PropertyChange propertyChange : prop) {
						String name = propertyChange.getName();
						Object val = propertyChange.getVal();
						
						if(name.equals("info.state")) {
							state = val.toString();
							break;
						}
					}
				}
			}
        	
		}
        
        return state;		
}
    public static void processTaskObjOfOperation(Task task, ServiceInstance si, OpResult result, Logger logger, String operation, String nodeName) throws InvalidProperty, RuntimeFault, RemoteException, InterruptedException
    {
    	String taskRef = task.getMOR().getVal();
        
        ObjectSpec objSpec = PropertyCollectorUtil.creatObjectSpec(task.getMOR(), Boolean.FALSE, null);
        
        PropertySpec pSpec = PropertyCollectorUtil.createPropertySpec(task.getMOR().getType(), false, new String[]{"info","info.state","info.result"});
        
        PropertyFilterSpec pfSpec = new PropertyFilterSpec();
        pfSpec.setObjectSet(new ObjectSpec[]{objSpec});
        pfSpec.setPropSet(new PropertySpec[]{pSpec});
                	        
        PropertyCollector pc = si.getServerConnection().getServiceInstance().getPropertyCollector();
        PropertyFilter pf = pc.createFilter(pfSpec, false);
        
        String version = "";
        
        String state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
        
        while (state.equals("unknown") || state.equals("running") || state.equals("queued")) {
        	Thread.sleep(500);
        	System.out.println("waiting for update in "+operation+" ....."+nodeName);
        	state = PerformanceBenchmarkUtils.waitForUpdates(si, pc, version, taskRef);
		}
        
        if(state.equals("success")) {
        	result.setEndTime(new Date());
        	result.setOperationStatus(OperationStatus.SUCESS);
        	logger.info(operation+" successfull "+nodeName);
        } else {
        	result.setEndTime(new Date());
        	result.setOperationStatus(OperationStatus.ERROR);
        	logger.error(operation+" failed "+nodeName);
        }
        
        pf.destroyPropertyFilter();
    }

	public static void removeAllVmUnderHost(String hostName, String vcUrl) {
		int count = VMContainerAccessor.getSize();
		if (count > 1) {
			for (int i = 0; i < count; i++) {
				NodeDetails vm = VMContainerAccessor.getVm();
				if (!vm.getHostName().equalsIgnoreCase(hostName)) {
					VMContainerAccessor.add(vm);
				} else {
					VMContainerAccessor.addToRemovedVmsList(vm);
				}
			}
		} else {
			int count1 = VMContainerAccessor.getSize(vcUrl);
			for (int i = 0; i < count1; i++) {
				NodeDetails vm = VMContainerAccessor.getVm(vcUrl);
				if (!vm.getHostName().equalsIgnoreCase(hostName)) {
					VMContainerAccessor.add(vcUrl, vm);
				} else {
					VMContainerAccessor.addToRemovedVmsList(vcUrl,vm);
				}
			}
		}
	}
	
	public static void addAllVmUnderHost(String hostName, String vcUrl) {
		int count = VMContainerAccessor.getRemovedVmsListSize();
		if (count > 1) {
			for (int i = 0; i < count; i++) {
				NodeDetails vm = VMContainerAccessor.getFromRemovedVmsList();
				if (vm.getHostName().equalsIgnoreCase(hostName)) {
					VMContainerAccessor.add(vm);
				} else {
					VMContainerAccessor.addToRemovedVmsList(vm);
				}
			}
		} else {
			int count1 = VMContainerAccessor.getRemovedVmsListSize(vcUrl);
			for (int i = 0; i < count1; i++) {
				NodeDetails vm = VMContainerAccessor.getFromRemovedVmsList(vcUrl);
				if (vm.getHostName().equalsIgnoreCase(hostName)) {
					VMContainerAccessor.add(vcUrl, vm);
				} else {
					VMContainerAccessor.addToRemovedVmsList(vcUrl,vm);
				}
			}
		}
	}
	
	public static void waitTillNoOpsOnHost(String hostName) throws InterruptedException
	{
        while (HostContainerAccessor.isInHostsForVmOpsList(hostName)) {
        	Thread.sleep(500);
        	System.out.println("waiting for previous operation to complete on host : " + hostName);
		}
	}

}
