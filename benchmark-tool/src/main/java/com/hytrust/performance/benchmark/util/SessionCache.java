/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.util;

import java.util.HashMap;
import java.util.Map;

import com.vmware.vim25.mo.ServiceInstance;

/**
 * @author rkonde
 * 
 * It is cache to hold sessions with vCenter
 * */
public class SessionCache {

    private static Map<String, ServiceInstance> sessiomCache = new HashMap<String, ServiceInstance>();
    
    public static ServiceInstance getSI(String vCurl) {
        return sessiomCache.get(vCurl);
    }
    
    public static void add(String vCurl, ServiceInstance serviceInstance) {
        sessiomCache.put(vCurl, serviceInstance);
    }
}
