/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author rkonde
 * 
 * This class creates ApplicationContext used by all classes in application 
 * */

public class SpringInitializer {
	private static ApplicationContext context;
	
	static {
		context = new ClassPathXmlApplicationContext("perfApplication.xml");
	}
	
	public static ApplicationContext getApplicationContext() {
		return context;
	}
}
