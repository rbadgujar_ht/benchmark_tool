/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.work.distribute;

import com.hytrust.performance.benchmark.operations.AbstractOperation;

/**
 * @author rbadgujar
 *
 */
public interface IOperationRandomizer {

    AbstractOperation getOperation();
}
