/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.work.distribute;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.enums.OperationTypes;
import com.hytrust.performance.benchmark.operations.AbstractOperation;
import com.hytrust.performance.benchmark.operations.OperationFactory;
import com.hytrust.performance.benchmark.properties.OperationDetails;

/**
 * Implementation of IOperationRandomizer to select an operation by a worker thread. 
 * @author rbadgujar
 *
 */
@Service
public class OperationRandomizerImpl implements IOperationRandomizer{

    private OperationDetails operationDetails;
    private Random random = null;
    private int upperRandomLimit = 0;
    private List<OperationTypes> startedOperationsList = null; 
    
    @Autowired
    public OperationRandomizerImpl(OperationDetails operationDetails) {
        this.operationDetails = operationDetails;
        startedOperationsList = new ArrayList<OperationTypes>(operationDetails.getMapOfConfiguredOpsNoOfOps().keySet());
        for (OperationTypes operationType : startedOperationsList) {
            startedOperationsMap.put(operationType, new AtomicInteger(operationDetails.getMapOfConfiguredOpsNoOfOps().get(operationType)));
        }
        random = new Random();
        upperRandomLimit = startedOperationsList.size();
    }
    
    public static AtomicInteger lastOperation = null;
    Map<OperationTypes, AtomicInteger> startedOperationsMap = new ConcurrentHashMap<OperationTypes, AtomicInteger>();
    public static AtomicInteger totalOps = new AtomicInteger();
    
    /* 
     * Selects & returns an Operation randomly. 
     * It calculates % completed of an operation and checks this value with the % specified in config file such that
     * the number of operations started at any time never surpasses the % of operation specified in config file. 
     * (non-Javadoc)
     * @see com.hytrust.performance.benchmark.work.distribute.IOperationRandomizer#getOperation()
     */
    @Override
    public AbstractOperation getOperation() {  
        AbstractOperation op = null;
        boolean valueSet = false;
        
        while(!valueSet) {
            //Randomly choose an operation type from a list of specified operations
            OperationTypes opType = startedOperationsList.get(random.nextInt(upperRandomLimit));
            
            int totalOpsThisType = operationDetails.getMapOfConfiguredOpsNoOfOps().get(opType);
            if(opType == null || totalOpsThisType == 0) {
                continue;
            }
            int remainingOps = startedOperationsMap.get(opType).get();
            int completedOpsOfThisType = totalOpsThisType - remainingOps;
            Double opsCompletedAsPercentageOfTotalOperations = (totalOps.doubleValue() == 0.0)?0:
                (completedOpsOfThisType*100.0)/totalOps.doubleValue();
            if(remainingOps > 0 && 
                    operationDetails.getValue(opType)>=opsCompletedAsPercentageOfTotalOperations) {
                startedOperationsMap.get(opType).decrementAndGet();
                op = OperationFactory.getOperation(opType);
                totalOps.incrementAndGet();
                valueSet = true;
            }
        }
        return op;
    }

    
}
