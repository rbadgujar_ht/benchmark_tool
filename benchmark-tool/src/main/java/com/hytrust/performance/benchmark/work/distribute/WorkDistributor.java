/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.work.distribute;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hytrust.performance.benchmark.constants.Constants;
import com.hytrust.performance.benchmark.properties.ConnectionDetails;
import com.hytrust.performance.benchmark.properties.OperationDetails;
import com.hytrust.performance.benchmark.properties.UserDetails;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.hytrust.performance.benchmark.work.execution.SeqWorker;
import com.hytrust.performance.benchmark.work.execution.Worker;

/**
 * @author rbadgujar
 *
 */
@Service
public class WorkDistributor {

    ExecutorService executor = null;
    
    @Autowired
    private OperationDetails operationDetails;
    
    @Autowired
    private IOperationRandomizer operationRandomizer;
    
    /**
     * Starts appropriate worker threads considering :
     * 1. Type of execution
     * 2. Total number of operations if parallel execution 
     * 3. Total number of user sessions if sequential execution
     * @param operationDetails
     * @param operationRandomizer
     */
    public void distributeTasks() {
        int maxParallelThreads = PerformanceBenchmarkUtils.getMaxRunnableThreads();
        executor = Executors.newFixedThreadPool(maxParallelThreads);
        String execType = operationDetails.getExecutionType();
        if(execType.equals(Constants.PARALLEL)) {
            for(int i=0;i<operationDetails.getTotalNoOfOperation();i++) {
                executor.submit(new Worker(operationRandomizer));
            }
        } else if (execType.equals(Constants.SEQ)) {
            int userSessionsCount = SpringInitializer.getApplicationContext().getBean(UserDetails.class).getNoOfUserSessions();
            ConnectionDetails conDetails =  SpringInitializer.getApplicationContext().getBean(ConnectionDetails.class);
            int noOfVc = conDetails.getNoOfVcenters();
            //Calculate number of operations to be performed per user session
            int opsPerUserSession = operationDetails.getTotalNoOfOperation()/userSessionsCount;
            int diff = operationDetails.getTotalNoOfOperation() % userSessionsCount;
            
            for(int i=0;i<userSessionsCount;i++) {
                //Get a vcUrl from cache or somewhere 
            	//For simplicity right now it works by selecting vc in round robin fashion
            	//Can be moved to randomizer as a method
                String vCurl = conDetails.getvCenters().get(i % noOfVc).getUrl();
                if (diff != 0) {
                    diff--;
                    executor.submit(new SeqWorker(operationRandomizer, vCurl, opsPerUserSession + 1));
                } else {
                    executor.submit(new SeqWorker(operationRandomizer, vCurl, opsPerUserSession));
                }
            }
        }
    }
    
    public void shutDown() {
    	executor.shutdown();
    }
    
    public boolean isTerminated() throws InterruptedException {
    	return executor.awaitTermination(10, TimeUnit.MINUTES);
    }
} 
