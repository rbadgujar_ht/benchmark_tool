/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.work.execution;

import java.util.List;
import java.util.concurrent.Callable;

import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.hytrust.performance.benchmark.init.HostContainer;
import com.hytrust.performance.benchmark.init.HostContainerAccessor;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.operations.AddHostOperation;
import com.hytrust.performance.benchmark.operations.CloneVmOperation;
import com.hytrust.performance.benchmark.operations.CreateVmOperation;
import com.hytrust.performance.benchmark.operations.DeleteHostOperation;
import com.hytrust.performance.benchmark.operations.DeleteVmOperation;
import com.hytrust.performance.benchmark.operations.DisconnectHostOperation;
import com.hytrust.performance.benchmark.operations.AbstractOperation;
import com.hytrust.performance.benchmark.operations.MigrateVmOperation;
import com.hytrust.performance.benchmark.operations.PowerOffVmOperation;
import com.hytrust.performance.benchmark.operations.PowerOnVmOperation;
import com.hytrust.performance.benchmark.operations.RebootHostOperation;
import com.hytrust.performance.benchmark.operations.RebootVmOperation;
import com.hytrust.performance.benchmark.operations.ReconnectHostOperation;
import com.hytrust.performance.benchmark.operations.RelocateVmOperation;
import com.hytrust.performance.benchmark.operations.SnapshotVmOperation;
import com.hytrust.performance.benchmark.properties.ConnectionDetails;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;
import com.hytrust.performance.benchmark.util.SessionCache;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.hytrust.performance.benchmark.work.distribute.IOperationRandomizer;
import com.vmware.vim25.mo.ServiceInstance;

/**
 * Worker thread for sequential execution of operations. One thread per user session. Shares vCenter login.
 * @author rbadgujar
 *
 */
public class SeqWorker implements Callable<String> {

    IOperationRandomizer operationRandomizer;
    String vcUrl = "";
    Integer opsPerUserSession = 0;
    public SeqWorker(IOperationRandomizer operationRandomizer, String vcUrl, Integer opsPerUserSession) {
        this.operationRandomizer = operationRandomizer;
        this.vcUrl = vcUrl;
        this.opsPerUserSession = opsPerUserSession;
    }
    
    @Override
    public String call() throws Exception {
        // Get service instance from cache
        ServiceInstance serviceInstance = SessionCache.getSI(vcUrl);
        
        VCenterDetails vcDetails = null;
        ConnectionDetails conDetails = SpringInitializer.getApplicationContext().getBean(ConnectionDetails.class);
        List<VCenterDetails> vcs = conDetails.getvCenters();
        for (VCenterDetails vc : vcs) {
			if(vcUrl.equals(vc.getUrl())) {
				vcDetails = vc;
				break;
			}
		}
        //Run this thread for a predefined number of time i.e. operations per user session
        while(opsPerUserSession > 0) {
            // Get operation from operationRandomizer
            AbstractOperation operation = operationRandomizer.getOperation();
            NodeDetails vmDetails = null;
            
            if(operation instanceof CreateVmOperation) {
            	vmDetails = new NodeDetails(vcUrl, PerformanceBenchmarkUtils.getVmName(), vcDetails);
            }
            if(operation instanceof DeleteVmOperation || operation instanceof CloneVmOperation || operation instanceof MigrateVmOperation || operation instanceof PowerOffVmOperation || operation instanceof PowerOnVmOperation || operation instanceof RebootVmOperation || operation instanceof RelocateVmOperation || operation instanceof SnapshotVmOperation ){
            	vmDetails = VMContainerAccessor.getVm(vcUrl);            	
            }
            
        	if(vmDetails != null) {
        		operation.setVmDetails(vmDetails);
            	operation.setSession(serviceInstance);  
            	if(vmDetails.getHostName() != null){
                	HostContainerAccessor.addToHostsForVmOps(vmDetails.getHostName());
            	}
        		operation.execute();
        		HostContainerAccessor.removeFromHostsForVmOps(vmDetails.getHostName());
            }else {
        		vmDetails = new NodeDetails(vcUrl, vcDetails);
        		operation.setVmDetails(vmDetails);
        		operation.setSession(serviceInstance);
        		operation.execute();
        	}
        	--opsPerUserSession;
        }
        return null;
    }

}
