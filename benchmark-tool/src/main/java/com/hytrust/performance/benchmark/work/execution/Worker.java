/*
 * Copyright (c) 2014 HyTrust Inc. All rights reserved.
 */

package com.hytrust.performance.benchmark.work.execution;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import com.hytrust.performance.benchmark.beans.VCenterDetails;
import com.hytrust.performance.benchmark.beans.NodeDetails;
import com.hytrust.performance.benchmark.init.VMContainerAccessor;
import com.hytrust.performance.benchmark.operations.AddHostOperation;
import com.hytrust.performance.benchmark.operations.CreateVmOperation;
import com.hytrust.performance.benchmark.operations.AbstractOperation;
import com.hytrust.performance.benchmark.operations.DeleteHostOperation;
import com.hytrust.performance.benchmark.operations.DisconnectHostOperation;
import com.hytrust.performance.benchmark.operations.RebootHostOperation;
import com.hytrust.performance.benchmark.operations.ReconnectHostOperation;
import com.hytrust.performance.benchmark.properties.ConnectionDetails;
import com.hytrust.performance.benchmark.util.PerformanceBenchmarkUtils;
import com.hytrust.performance.benchmark.util.SpringInitializer;
import com.hytrust.performance.benchmark.work.distribute.IOperationRandomizer;

/**
 * Worker thread for parallel execution. Each thread executes an operation independently. 
 * @author rbadgujar
 *
 */
public class Worker implements Callable<String>{

    IOperationRandomizer operationRandomizer;
    public Worker(IOperationRandomizer operationRandomizer) {
        this.operationRandomizer = operationRandomizer;
    }

    public String call() throws Exception {
        AbstractOperation operation = operationRandomizer.getOperation();
        
        VCenterDetails vcDetails = null;
        ConnectionDetails conDetails = SpringInitializer.getApplicationContext().getBean(ConnectionDetails.class);
        List<VCenterDetails> vcs = conDetails.getvCenters();
        
        //Will randomly pick VC. We can have more specific code to pick VC Separately
        Random r =new Random();
        vcDetails = vcs.get(r.nextInt(conDetails.getNoOfVcenters()));
        
        NodeDetails vmDetails = null;
        if(operation instanceof CreateVmOperation) {
        	vmDetails = new NodeDetails(vcDetails.getUrl(), PerformanceBenchmarkUtils.getVmName(), vcDetails);
        }
        else {
        	vmDetails = VMContainerAccessor.getVm();
        }
                
        if(vmDetails != null) {
        	operation.setVmDetails(vmDetails);
        	operation.execute();
        }
        else{
    		vmDetails = new NodeDetails(vcDetails.getUrl(), vcDetails);
    		operation.setVmDetails(vmDetails);
    		operation.execute();
    	}
        return "SUCCESS";
    }
}
